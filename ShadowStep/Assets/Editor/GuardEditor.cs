﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class GuardEditor : EditorWindow
{
    static private Vector2 m_windowMinSize = new Vector2(300, 300);
    GameObject m_lastGuard = null;
    GameObject m_guardPrefab = null;

    GuardManager m_guardManager = null;

    private void OnEnable()
    {
        m_guardPrefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Enemy Prefabs/Guard.prefab", typeof(GameObject));
        m_guardManager = FindObjectOfType<GuardManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetCurrentSelectedGuard())
            m_lastGuard = GetCurrentSelectedGuard();
    }

    GameObject GetCurrentSelectedGuard()
    {
        GameObject go = Selection.activeGameObject;

        if (go != null)
            if (go.GetComponent<GuardLogic>())
                return go;

        return null;
    }

    void OnGUI()
    {
        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.BeginHorizontal();
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUIContent gUIContent = new GUIContent("+", "Add Checkpoint to current selected guard");
        if (GUILayout.Button(gUIContent))
        {
            if (m_lastGuard)
            {
                m_lastGuard.GetComponent<GuardLogic>().CreateCheckPoint();
                EditorUtility.SetDirty(m_lastGuard.GetComponent<GuardLogic>());
            }

        }
        GUIContent gUIContent2 = new GUIContent("-", "Remove last Checkpoint of current selected guard");
        if (GUILayout.Button(gUIContent2))
        {
            if (m_lastGuard)
            {
                m_lastGuard.GetComponent<GuardLogic>().RemoveCheckPoint();
                EditorUtility.SetDirty(m_lastGuard.GetComponent<GuardLogic>());
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUIContent gUIContent3 = new GUIContent("Create Guard", "Creates a new Guard");
        if(GUILayout.Button(gUIContent3))
        {
            m_guardManager.CreateGuard(m_guardPrefab);
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (m_lastGuard)
            GUILayout.Label("Info: " + m_lastGuard.name + " is selected!");
        else
            GUILayout.Label("Info: No guards is selected!");

        Repaint();
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }

    [MenuItem("Custom/Guard Editor", false, 4)]
    static void CreateGuardEditor()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(GuardEditor));
        window.title = "Guard Editor";
        window.minSize = m_windowMinSize;
        window.Show();
    }
}
