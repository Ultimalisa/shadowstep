﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(FieldOfView))]
public class FieldOfViewEditor : UnityEditor.Editor
{

    void OnSceneGUI()
    {
        FieldOfView Fov = (FieldOfView)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(Fov.transform.position, -Fov.transform.forward, Fov.transform.up, 360, Fov.m_viewRadius);
        Vector3 viewAngleA = Fov.DirFromAngle(-Fov.m_viewAngle / 2, true);
        Vector3 viewAngleB = Fov.DirFromAngle(Fov.m_viewAngle / 2, true);

        Handles.DrawLine(Fov.transform.position, Fov.transform.position + viewAngleA * Fov.m_viewRadius);
        Handles.DrawLine(Fov.transform.position, Fov.transform.position + viewAngleB * Fov.m_viewRadius);
    }

}
