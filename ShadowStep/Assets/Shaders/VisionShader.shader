﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/VisionShader"
{
    Properties
    {
		/*_Alpha("Alpha", Range(0,1)) = 1.0*/
		/*_CircleColor("Circle Color", Color) = (0,0,0,1)
		_BgColor("Background Color", Color) = (1,0,0,1)*/
		_border("Anti Alias Border Threshold", Range(0.00001, 50)) = 0.01
		_Radius("Radius", Range(0, 100)) = 50.0
		_MainTex("Main Texture", 2D) = "white" {}
		_EffectAmount("EffectAmount", Range(0, 1)) = 1.0
    }

    SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			// variables
			/*float _Alpha;*/
			float _border;
			float _Radius;
			/*fixed4 _CircleColor;
			fixed4 _BgColor;*/
			sampler2D _MainTex;
			uniform float _EffectAmount;

			v2f vert(appdata v)
			{
				v2f o;				
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;

				return o;
			}

			float2 antialias(float radius, float borderSize, float dist)
			{
				float t = smoothstep(radius + borderSize, radius - borderSize, dist);
				return t;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float4 col;
				float2 center = _ScreenParams.xy / 2;

				float maxradius = length(center);
				float radius = maxradius * (_Radius / 100);
				float dis = distance(i.vertex, center);

				/*_CircleColor.a = _Alpha;*/

				fixed4 grayScaleCol = tex2D(_MainTex, i.uv);
				fixed4 renderTexCol = tex2D(_MainTex, i.uv);

				
				grayScaleCol.rgb = lerp(grayScaleCol.rgb, dot(grayScaleCol.rgb, float3(0.2126, 0.7152, 0.0722)), _EffectAmount);

				//float g = 0.3 * grayScaleCol.r + 0.59 * grayScaleCol.g + 0.11 * grayScaleCol.b;

				/*grayScaleCol.r = g * grayScaleCol.r;
				grayScaleCol.g = g * grayScaleCol.g;
				grayScaleCol.b = g * grayScaleCol.b;
				grayScaleCol.a = 1;*/

				float aliasVal = antialias(radius, _border, dis);
				col = lerp(grayScaleCol, renderTexCol, aliasVal);

				return col;

			}

			ENDCG
		}
	}
}
