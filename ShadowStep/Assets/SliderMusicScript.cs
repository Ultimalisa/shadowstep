﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderMusicScript : MonoBehaviour
{
    Slider m_slider;
    // Start is called before the first frame update
    void Start()
    {
        m_slider = GetComponent<Slider>();
        m_slider.onValueChanged.AddListener(delegate { SetVolume(m_slider.value); });


        if (PlayerPrefs.GetFloat("MusicVolume") > 0f)
        {
            m_slider.value = PlayerPrefs.GetFloat("MusicVolume");
            GameManager.Instance.m_audioManager.SetVolumeMusic(m_slider.value);
        }
    }

    void SetVolume(float vol)
    {
        //GameManager.Instance.m_audioManager.m_sfxVolume = vol;
        //Debug.Log("vol: " + GameManager.Instance.m_audioManager.m_sfxVolume);
        GameManager.Instance.m_audioManager.SetVolumeMusic(vol);

        PlayerPrefs.SetFloat("MusicVolume", vol);
    }
}
