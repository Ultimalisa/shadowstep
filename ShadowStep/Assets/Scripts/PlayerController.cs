﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public bool m_isShadowStepping = false;
    [HideInInspector]
    public bool m_allowShadowStepping = false;
    public LayerMask m_obstacleMask;

    // The Rewired player id of this character
    private int m_playerId = 0;

    // The movement speed of this character
    [SerializeField]
    internal float m_moveSpeed = 3.0f;

    [SerializeField]
    internal float m_dashDuration = 0.5f;
    [SerializeField]
    internal float m_dashDistance = 10f;
    [SerializeField]
    internal float m_dashOffsetToObstacles;

    // allow control via Keyboard? if false the Gamepad will take control over our Player
    internal bool m_useKeyboard = true;

    private Rigidbody2D m_rb;
    [SerializeField]
    internal float m_acceleration = 0.001f;
    [SerializeField]
    internal float m_accelerationSpeed = 1f;
   
    //private bool ActiveStatus = true;
    public GameObject m_map;
   
    private Rewired.Player m_player; // The Rewired Player
    private Vector2 m_moveVector;
    [HideInInspector]
    public Vector3 m_playerStartPos;
    [HideInInspector]
    public Quaternion m_playerStartRot;
    public static bool m_butPressed = false;
    public GameObject m_shadow;
    public GameObject m_lightCanvas;
    [HideInInspector]
    public float m_score;
    public static bool m_mapOpen = false;
    private LineRenderer m_lineRenderer;
    private Vector2 m_startDashPos;
    private Vector2 m_endDashPos;
    public Animator m_animator;
    public AudioSource m_audioShadowStep;
    public static bool m_interacted =false;
    private float m_currentLerpTime;
    [HideInInspector]
    public string m_generatorName;
    private bool m_dashParticles = true;

    void Awake()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        m_player = ReInput.players.GetPlayer(m_playerId);     
    }

    private void Start()
    {
       
        m_animator = GetComponent<Animator>();
        m_rb = transform.GetComponent<Rigidbody2D>();
        m_shadow.SetActive(false);
        m_lightCanvas.SetActive(false);
        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(gameObject.GetComponent<AudioSource>());

        m_animator.SetBool("IsWalking", false);
        m_animator.SetBool("IsDashing", false);
        GameManager.Instance.m_menuCanvas.SetActive(false);
        if (GameManager.Instance.m_menuCanvas.transform.GetChild(6).gameObject != null)
            GameManager.Instance.m_menuCanvas.transform.GetChild(6).gameObject.SetActive(false);

        m_playerStartPos = transform.localPosition;
        m_playerStartRot = transform.localRotation;

        m_lineRenderer = GetComponent<LineRenderer>();

        GameManager.Instance.m_player = this;
        GameManager.Instance.m_originalPlayerMoveSpeed = m_moveSpeed;
    }

    private void Update()
    {
        //reset move vector for this frame
         m_moveVector = Vector2.zero;

        if (!m_isShadowStepping)
        {
            m_animator.SetBool("IsWalking", true);
            m_animator.SetBool("IsDashing", false);
            if(m_dashParticles)
            {
                transform.GetChild(2).gameObject.GetComponent<ParticleSystem>().Stop();
                m_dashParticles = false;
            }
           
        }
        if (m_isShadowStepping)
        {
            if(!m_dashParticles)
            {
                transform.GetChild(2).gameObject.GetComponent<ParticleSystem>().Play();
                m_dashParticles = true;
            }
           
        }
        if (m_player.GetButtonDown("Option"))
        {
            GameManager.Instance.m_isGameRunning = !GameManager.Instance.m_isGameRunning;

            if (!GameManager.Instance.m_isGameRunning)
            {
                GameManager.Instance.m_menuCanvas.SetActive(true);
                GameManager.Instance.m_menuCanvas.transform.GetChild(6).gameObject.SetActive(false);
            }
            else
            {
                FindObjectOfType<ButtonBehavior>().CloseMenu();
            }
        }
       

        if (m_allowShadowStepping==true)
        {
            m_shadow.SetActive(true);
        }else m_shadow.SetActive(false);

        if (LightField.m_isStandingInLightField == true)
        {
            m_lightCanvas.SetActive(true);
        }else m_lightCanvas.SetActive(false);

        if (GameManager.Instance.m_isGameRunning)
        {
            FaceJoystick();
            GetInput();

            /* if(m_moveVector.x==0 && m_moveVector.y == 0)
             {
                 animator.SetBool("IsWalking", false);
                 animator.SetBool("IsDashing", false);
             }*/
        }
    }

    void FaceJoystick()
    {
        float xRotation = -Input.GetAxisRaw("Horizontal");
        float yRotation = -Input.GetAxisRaw("Vertical");
        Vector3 stickPos = new Vector3(xRotation, yRotation, 0f);

        //stickPos = Camera.main.ScreenToWorldPoint(stickPos);
        //Vector2 direction = new Vector2(stickPos.x - transform.position.x, stickPos.y - transform.position.y);
        //transform.up = direction.normalized;
       
        if (stickPos != Vector3.zero)
        {
            Quaternion playerRot = transform.rotation;
            float curRotAngleY = playerRot.eulerAngles.y;
            curRotAngleY = Mathf.Atan2(stickPos.y, stickPos.x);
            Vector3 rotAngles = playerRot.eulerAngles;
            rotAngles.z = curRotAngleY * Mathf.Rad2Deg + 90f;
            playerRot.eulerAngles = rotAngles;
           
            transform.rotation = playerRot;
        }
    }

    void FixedUpdate()
    {
        if (GameManager.Instance.m_isGameRunning)
        {
            m_rb.velocity = Vector3.zero;
            m_rb.angularVelocity = 0;

            if (m_isShadowStepping)
            {
                //Lerp
                m_currentLerpTime += Time.deltaTime;
                Mathf.Clamp(m_currentLerpTime, 0f, m_dashDuration);
                float perc = m_currentLerpTime / m_dashDuration;
                perc = perc * perc;

                transform.position = Vector2.Lerp(m_startDashPos, m_endDashPos, perc);

                if(perc >= 1)
                {
                    ResetShadowStep();
                }
            }
            else
            {
                ProcessInput();
            }
        }
    }

    private void ProcessInput()
    {
        // Process movement
        if ((m_moveVector.x != 0.0f || m_moveVector.y != 0.0f) && !m_player.GetButton("ShadowStep"))
        {
            m_acceleration = Mathf.Clamp(Mathf.Lerp(m_acceleration, 1f, Time.deltaTime * m_accelerationSpeed), 0f, 1f);
            m_rb.MovePosition(Vector3.Lerp(m_rb.position, m_rb.position + m_moveVector.normalized, Time.deltaTime * m_moveSpeed * m_acceleration));
        }
        else
            m_acceleration = 0f;
    }

    private void GetInput()
    {
        m_useKeyboard = LevelManager.Instance.m_isShowingMouse;

        //Keyboard
        if (m_useKeyboard)
        {
            if (m_player.GetButton("Right"))
                m_moveVector.x = 1;
         
            if (m_player.GetButton("Left"))
                m_moveVector.x = -1;
              
            if (m_player.GetButton("Up")) 
                m_moveVector.y = 1;
              
            if (m_player.GetButton("Down"))
                m_moveVector.y = -1;
        }

        // Controller
        else
        {
            m_moveVector.x = m_player.GetAxis("MoveHorizontal"); // get input by name or action id
            m_moveVector.y = m_player.GetAxis("MoveVertical");
        }

        if(m_allowShadowStepping)
            m_player.controllers.maps.SetMapsEnabled(true, "SS");
        else
            m_player.controllers.maps.SetMapsEnabled(false, "SS");

        if (m_player.GetButton("ShadowStep") && !m_isShadowStepping)
        {
            if (m_moveVector != Vector2.zero)
            {
                m_lineRenderer.enabled = true;

                RaycastHit2D ray = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), m_moveVector.normalized, m_dashDistance, m_obstacleMask);

                if (ray.collider)
                {
                    m_endDashPos = ray.point + ray.normal * m_dashOffsetToObstacles;
                }
                else
                    m_endDashPos = new Vector2(transform.position.x, transform.position.y) + m_moveVector.normalized * m_dashDistance;

                //Show ray of dash
                m_lineRenderer.SetPosition(0, transform.position);
                m_lineRenderer.SetPosition(1, m_endDashPos);
            }
            else
                m_lineRenderer.enabled = false;
        }
        if(m_player.GetButtonUp("ShadowStep") && !m_isShadowStepping)
        {
            if (m_moveVector != Vector2.zero)
            {
                m_audioShadowStep.Play();
                m_animator.SetBool("IsDashing", true);
                m_animator.SetBool("IsWalking", false);
                m_startDashPos = transform.position;
                m_isShadowStepping = true;
            }

            m_lineRenderer.enabled = false;
        }
        if (m_player.GetButtonDown("Map"))
        {
            m_mapOpen = true;
        }
        else m_mapOpen = false;

        if (m_player.GetButton("Map"))
        {
           
            m_map.SetActive(true);
        }
       
        if (m_player.GetButtonUp("Map"))
        {
            m_map.SetActive(false);
        }
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, 4f);
       
        if (m_player.GetButtonDown("Interact") && hit.collider != null)
        {
            if(hit.collider.gameObject.name.Contains("Gen"))
            {
                m_butPressed = true;
                m_generatorName = hit.collider.gameObject.name;
            }
           
        }
        else m_butPressed = false;
        if (m_player.GetButtonDown("Interact"))
        {
            m_interacted = true;
        }

        //if (m_player.GetButtonDown("Option"))
        //{
        //    Debug.Log("Option");
        //    if (ActiveStatus)
        //    {
        //        OptionCanvas.SetActive(true);
        //        Time.timeScale = 0f;
        //        ActiveStatus = false;
        //    }
        //    else
        //    {
        //        OptionCanvas.SetActive(false);
        //        Time.timeScale = 1f;
        //        ActiveStatus = true;
        //    }
        //}
    }

    public void ResetShadowStep()
    {
        m_currentLerpTime = 0;
        m_isShadowStepping = false;

        m_animator.SetBool("IsWalking", true);
        m_animator.SetBool("IsDashing", false);
    }
}
