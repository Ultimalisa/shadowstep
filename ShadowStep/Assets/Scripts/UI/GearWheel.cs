﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearWheel : MonoBehaviour
{
    //[SerializeField]
    //internal GameObject m_gearWheel;
    [SerializeField]
    internal float m_rotSpeed = -1f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0f, 0f, m_rotSpeed));
    }
}
