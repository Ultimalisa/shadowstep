﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuController : MonoBehaviour
{
    public GameObject m_optionCanvas;
    [SerializeField]
    internal GameObject CreditsCanvas;
    public Button m_startButton;
    public Button m_optionButton;
    public Button m_quitGameButton;
    public Button m_backButton;
    public Button m_creditsButton;
    public Button BackButtonFromCredits;
    public Transform m_effectMenu;
    public Transform m_effectOption;

    internal GraphicRaycaster m_gr;

    List<Button> m_allMenuButtonList = new List<Button>();
    List<Button> m_menuButtonList = new List<Button>();
    List<Button> m_optionButtonList = new List<Button>();

    private void Awake()
    {
        m_startButton.onClick.AddListener(StartGame);
        m_optionButton.onClick.AddListener(OpenOptions);
        m_quitGameButton.onClick.AddListener(Exit);
        m_backButton.onClick.AddListener(BackToMenuCanvas);
        m_creditsButton.onClick.AddListener(OpenCredits);
        BackButtonFromCredits.onClick.AddListener(BackToMenuCanvasFromCredits);
        m_optionCanvas.SetActive(false);
        CreditsCanvas.SetActive(false);
        m_gr = GetComponent<GraphicRaycaster>();

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<Button>())
            {
                m_allMenuButtonList.Add(transform.GetChild(i).GetComponent<Button>());
                m_menuButtonList.Add(transform.GetChild(i).GetComponent<Button>());
            }
        }

        for (int i = 0; i < m_optionCanvas.transform.childCount; i++)
        {
            if (m_optionCanvas.transform.GetChild(i).GetComponent<Button>())
            {
                m_optionButtonList.Add(m_optionCanvas.transform.GetChild(i).GetComponent<Button>());
                m_allMenuButtonList.Add(m_optionCanvas.transform.GetChild(i).GetComponent<Button>());
            }
        }

        m_allMenuButtonList.Add(CreditsCanvas.transform.GetChild(0).GetComponent<Button>());

        DisableButtons();
        EnableMenuButtons();
    }

    private void Update()
    {
        m_effectMenu.Rotate(new Vector3(0f, 0f, -1f));
        m_effectOption.Rotate(new Vector3(0f, 0f, -1f));

        if (LevelManager.Instance.m_isShowingMouse)
        {
            LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
            if(LevelManager.Instance.m_eventSystem.IsPointerOverGameObject())
            {
                PointerEventData ped = new PointerEventData(LevelManager.Instance.m_eventSystem);
                ped.position = Input.mousePosition;
                List<RaycastResult> rrl = new List<RaycastResult>();
                m_gr.Raycast(ped, rrl);
                if(rrl.Count > 0)
                {
                    Vector2 lerpPos = m_effectMenu.transform.localPosition;
                    lerpPos.y = Mathf.Lerp(lerpPos.y, rrl[0].gameObject.transform.localPosition.y, Time.deltaTime * 2f);
                    m_effectMenu.localPosition = lerpPos;

                    Vector2 lerpPos2 = m_effectOption.transform.position;
                    lerpPos2.y = Mathf.Lerp(lerpPos2.y, rrl[0].gameObject.transform.position.y, Time.deltaTime * 4f);
                    m_effectOption.position = new Vector3(m_effectOption.position.x, lerpPos2.y, m_effectOption.position.z);

                    //Debug.Log(rrl[0].gameObject.name);
                }
            }
        }
        else
        {
            if (LevelManager.Instance.m_eventSystem.currentSelectedGameObject == null)
            {
                List<Button> buttons = GetActiveButtons();
                LevelManager.Instance.m_eventSystem.SetSelectedGameObject(buttons[0].gameObject);
            }

            Vector2 lerpPos = m_effectMenu.transform.localPosition;
            lerpPos.y = Mathf.Lerp(lerpPos.y, LevelManager.Instance.m_eventSystem.currentSelectedGameObject.transform.localPosition.y, Time.deltaTime * 2f);
            m_effectMenu.localPosition = lerpPos;

            Vector2 lerpPos2 = m_effectOption.transform.localPosition;
            lerpPos2.y = Mathf.Lerp(lerpPos2.y, LevelManager.Instance.m_eventSystem.currentSelectedGameObject.transform.localPosition.y, Time.deltaTime * 4f);
            m_effectOption.localPosition = new Vector3(m_effectOption.position.x, lerpPos2.y, m_effectOption.position.z);
        }
    }

    void StartGame()
    {
       
       
        LevelManager.Instance.LoadScene("LevelSelect");
    }

    public void Exit()
    {
        Application.Quit();
    }

    void BackToMenuCanvasFromCredits()
    {
        EnableMenuButtons();
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(transform.GetChild(1).gameObject);
        CreditsCanvas.SetActive(false);
        Debug.Log("Is Clicked Back");
    }

    void OpenCredits()
    {
        CreditsCanvas.SetActive(true);
        DisableButtons();
        CreditsCanvas.transform.GetChild(0).GetComponent<Button>().enabled = true;
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(CreditsCanvas.transform.GetChild(0).gameObject);

    }

    void OpenOptions()
    {
        m_effectOption.localPosition = new Vector2(m_effectOption.localPosition.x, m_backButton.transform.localPosition.y); 
        m_optionCanvas.SetActive(true);
        DisableButtons();

        for (int i = 0; i < m_menuButtonList.Count; i++)
            m_menuButtonList[i].gameObject.GetComponent<Image>().raycastTarget = false;

        EnableOptionButtons();
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(m_optionCanvas.transform.GetChild(1).gameObject);

        Debug.Log("Is Clicked");

    }
    void BackToMenuCanvas()
    {
        EnableMenuButtons();
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(transform.GetChild(1).gameObject);
        m_effectMenu.localPosition = new Vector2(m_effectMenu.localPosition.x, m_startButton.transform.localPosition.y);
        m_optionCanvas.SetActive(false);
        Debug.Log("Is Clicked Back");
    }

    void DisableButtons()
    {
        foreach (Button i in m_allMenuButtonList)
        {
            i.enabled = false;
        }
    }

    void EnableMenuButtons()
    {
        m_effectMenu.GetComponent<Image>().enabled = true;

        foreach (Button i in m_menuButtonList)
        {
            i.enabled = true;
            i.gameObject.GetComponent<Image>().raycastTarget = true;
        }
    }

    void EnableOptionButtons()
    {
        foreach (Button i in m_optionButtonList)
        {
            i.enabled = true;
        }
    }

    List<Button> GetActiveButtons()
    {
        List<Button> m_activeButtonList = new List<Button>();
        foreach (Button i in m_allMenuButtonList)
        {
            if (i.enabled)
                m_activeButtonList.Add(i);
        }

        return m_activeButtonList;
    }
   
}
