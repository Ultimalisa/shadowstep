﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelCanvas : MonoBehaviour
{
   
    private int m_lastSelectedLevel;
    private int m_levelNumber;

    List<Button> m_buttonList = new List<Button>();
    GraphicRaycaster m_gr;

    // Start is called before the first frame update
    void Start()
    {
        m_gr = GetComponent<GraphicRaycaster>();
       
        m_levelNumber = GameManager.Instance.GetLevelNumber();

        for (int i = 1; i <= m_levelNumber; i++)
        {
            // make sure Hierarchy set up is correct, so we won't get null references!!
            int temp = i;

            m_buttonList.Add(transform.GetChild(i).GetComponent<Button>());
            m_buttonList[i-1].onClick.AddListener(delegate { LoadLevel(m_buttonList[temp - 1].gameObject.name); });
            transform.GetChild(i).GetComponent<Image>().enabled = false;

            string s = m_buttonList[temp - 1].gameObject.name;
            string ln = s.Substring(s.Length - 1, 1);

            // Load correct images for wheels
            transform.GetChild(i).transform.GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[PlayerPrefs.GetInt("KillWheel" + ln)]; //wheel 1
            transform.GetChild(i).transform.GetChild(1).transform.GetChild(1).GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[PlayerPrefs.GetInt("DeathWheel" + ln)]; //wheel 2
            transform.GetChild(i).transform.GetChild(1).transform.GetChild(2).GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[PlayerPrefs.GetInt("TimeWheel" + ln)]; //wheel 3

            // Disable Popup
            transform.GetChild(i).transform.GetChild(1).gameObject.SetActive(false);
        }

        m_lastSelectedLevel = PlayerPrefs.GetInt("lastSelectedLevel");

        if(m_lastSelectedLevel > 0 && m_lastSelectedLevel <= m_levelNumber)
        {
            transform.GetChild(m_lastSelectedLevel).GetComponent<Image>().enabled = true;
            transform.GetChild(m_lastSelectedLevel).transform.GetChild(1).gameObject.SetActive(true);

            // Controller?
            if (!LevelManager.Instance.m_isShowingMouse)
                LevelManager.Instance.m_eventSystem.SetSelectedGameObject(transform.GetChild(m_lastSelectedLevel).gameObject);
        }


    }

    // Update is called once per frame
    void Update()
    {
        // KB&M?
        if(LevelManager.Instance.m_isShowingMouse)
        {
            if (LevelManager.Instance.m_eventSystem.currentSelectedGameObject != null)
            {
                LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
            }

            PointerEventData ped = new PointerEventData(LevelManager.Instance.m_eventSystem);
            ped.position = Input.mousePosition;
           
            List<RaycastResult> rrl = new List<RaycastResult>();
            m_gr.Raycast(ped, rrl);

            if(rrl.Count > 0)
            {
                //Debug.Log(rrl[0].gameObject.transform.parent.name);

                // if Image of current highlighted Button is disabled
                if(rrl[0].gameObject.transform.parent.transform.GetComponent<Image>().enabled == false)
                {
                    // Disable all Button Images
                    for (int i = 1; i <= m_levelNumber; i++)
                    {
                        m_buttonList[i-1].gameObject.GetComponent<Image>().enabled = false;

                        // Disable Popup
                        transform.GetChild(i).transform.GetChild(1).gameObject.SetActive(false);
                    }

                    // Enable Image of current highlighted Button
                    rrl[0].gameObject.transform.parent.transform.GetComponent<Image>().enabled = true;
                    rrl[0].gameObject.transform.parent.transform.GetChild(1).gameObject.SetActive(true);

                    
                }
            }

        }

        // Controller?
        else
        {
           
            if (LevelManager.Instance.m_eventSystem.currentSelectedGameObject == null)
            {
                LevelManager.Instance.m_eventSystem.SetSelectedGameObject(transform.GetChild(1).gameObject);
                for(int i = 0; i < m_levelNumber; i++)
                {
                    m_buttonList[i].image.enabled = false;

                    // Disable Popup
                    m_buttonList[i].transform.GetChild(1).gameObject.SetActive(false);
                }
                m_buttonList[0].image.enabled = true;
                m_buttonList[0].transform.GetChild(1).gameObject.SetActive(true);
            }

            if(LevelManager.Instance.m_eventSystem.currentSelectedGameObject.GetComponent<Image>().enabled == false)
            {
                for (int i = 1; i <= m_levelNumber; i++)
                {
                    // Disable all Button Images
                    m_buttonList[i-1].gameObject.GetComponent<Image>().enabled = false;
                    // Disable Popup
                    transform.GetChild(i).transform.GetChild(1).gameObject.SetActive(false);

                    //Enable Image and Popup of current selected Button
                    LevelManager.Instance.m_eventSystem.currentSelectedGameObject.GetComponent<Image>().enabled = true;
                    LevelManager.Instance.m_eventSystem.currentSelectedGameObject.transform.GetChild(1).gameObject.SetActive(true);


                }
            }
        }
    }

    void LoadLevel(string level)
    {
        Debug.Log("clicked on Level" + level);

        string levelNumber = level.Substring(level.Length - 1, 1);

        PlayerPrefs.SetInt("lastSelectedLevel", int.Parse(levelNumber));
        LevelManager.Instance.LoadScene(level);
    }
}
