﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerProgress : MonoBehaviour
{
    [HideInInspector]
    public int[] playerDeaths;
    [HideInInspector]
    public int[] enemiesKilled;
    [HideInInspector]
    public float[] TimeInLevel;
 
  


    private void Start()
    {
        playerDeaths = new int[5];
        for (int i = 0; i < 5; i++)
        {
            playerDeaths[i] = 0;
        }
       
        enemiesKilled = new int[5];
        for (int i = 0; i < 5; i++)
        {
            enemiesKilled[i] = 0;
        }
       
        TimeInLevel = new float[5];
        for (int i = 0; i < 5; i++)
        {
            TimeInLevel[i] = 0;
        }
      
      
       

    }

    public void SetValues()
    {
            if ((TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]) < PlayerPrefs.GetFloat("TimeInLevel" + LevelManager.Instance.GetCurrentLevelNumber()) && PlayerPrefs.GetFloat("TimeInLevel" + LevelManager.Instance.GetCurrentLevelNumber()) > 0f)
            {
                PlayerPrefs.SetFloat("TimeInLevel" + LevelManager.Instance.GetCurrentLevelNumber(), TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]);
            }
            if (enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1] < PlayerPrefs.GetInt("Kills") && PlayerPrefs.GetInt("Kills") > 0)
            {
                PlayerPrefs.SetInt("Kills" + LevelManager.Instance.GetCurrentLevelNumber(), enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1]);
            }
            if (playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1] > PlayerPrefs.GetInt("Deaths" + LevelManager.Instance.GetCurrentLevelNumber()) && PlayerPrefs.GetInt("Deaths" + LevelManager.Instance.GetCurrentLevelNumber()) > 0)
            {
                PlayerPrefs.SetInt("Deaths" + LevelManager.Instance.GetCurrentLevelNumber(), playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1]);
            }
    }

   
    


}
