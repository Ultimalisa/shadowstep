﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBehaviour : MonoBehaviour
{
    public float m_rotationspeed;
    public float m_distance;
    public LayerMask m_targetMask;
    public int m_numberOfLines=1;
    public LineRenderer m_lineOfSight;
    public LineRenderer m_lineOfSight2;
    public Gradient m_greenColor;
    public Gradient m_redColor;
    public bool m_otherDir=false;
    private void Start()
    {
        Physics2D.queriesStartInColliders = false;
        if(m_numberOfLines==1)
        {
            m_lineOfSight2.enabled = false;
        }
        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(transform.GetChild(1).GetComponent<AudioSource>());
    }

    void Update()
    {
        if (GameManager.Instance.m_isGameRunning)
        {
            if(!m_otherDir)
                transform.Rotate(Vector3.forward * m_rotationspeed * Time.deltaTime);
            if (m_otherDir)
                transform.Rotate(-Vector3.forward * m_rotationspeed * Time.deltaTime);
            RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.right, m_distance, m_targetMask);
            RaycastHit2D hitInfo2 = Physics2D.Raycast(transform.position, -transform.right, m_distance, m_targetMask);
            if (hitInfo.collider != null)
            {
               
                m_lineOfSight.SetPosition(1, hitInfo.point);
                m_lineOfSight.colorGradient = m_greenColor;
               

                if (hitInfo.collider.CompareTag("Player")  && GameManager.Instance.m_player.m_isShadowStepping == false)
                {
                    
                    m_lineOfSight.SetPosition(1, hitInfo.point);
                    m_lineOfSight.colorGradient = m_redColor;
                    hitInfo.collider.gameObject.SetActive(false);
                    GameManager.Instance.InititateDeathSequence();
                    hitInfo.collider.gameObject.SetActive(true);

                }
            }
            else if (hitInfo.collider == null)
            {
              
                m_lineOfSight.SetPosition(1, transform.position + transform.right * m_distance);
                m_lineOfSight.colorGradient = m_greenColor;
               
            }
            if(hitInfo2.collider != null && m_numberOfLines==2)
            {
                m_lineOfSight2.SetPosition(1, hitInfo2.point);
                m_lineOfSight2.colorGradient = m_greenColor;

                if(hitInfo2.collider.CompareTag("Player") && GameManager.Instance.m_player.m_isShadowStepping == false)
                {
                    m_lineOfSight2.SetPosition(1, hitInfo2.point);
                    m_lineOfSight2.colorGradient = m_redColor;
                    hitInfo2.collider.gameObject.SetActive(false);
                    GameManager.Instance.InititateDeathSequence();
                    hitInfo2.collider.gameObject.SetActive(true);
                }
            }else if(hitInfo2.collider == null && m_numberOfLines==2)
            {
                m_lineOfSight2.SetPosition(1, transform.position - transform.right * m_distance);
                m_lineOfSight2.colorGradient = m_greenColor;
            }
        }
       
            m_lineOfSight.SetPosition(0, transform.position);
           if(m_numberOfLines==2)
            m_lineOfSight2.SetPosition(0, transform.position);
        
       
    }
}
