﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonBehavior : MonoBehaviour
{
    [SerializeField]
    internal GameObject m_optionCanvas;

    public Button m_levelSelectButton;
    public Button m_optionButton;
    public Button m_backButton;
    public Button m_exitButton;
    public Button m_continueButton;
    public Transform m_effectMenu;
    public Transform m_effectOption;

    List<Button> m_allMenuButtonList = new List<Button>();
    List<Button> m_menuButtonList = new List<Button>();
    List<Button> m_optionButtonList = new List<Button>();
    List<Button> m_clearButtonList = new List<Button>();

    Color m_selectedColor;
    Color m_normalColor;

    private GraphicRaycaster m_gr;

    private void OnEnable()
    {
        if(GameManager.Instance.m_clearScore.active)
        {
            for(int i = 0; i < m_clearButtonList.Count; i++)
            {
                m_clearButtonList[i].enabled = false;
            }

            LevelManager.Instance.m_eventSystem.SetSelectedGameObject(m_continueButton.gameObject);
        }
    }

    private void OnDisable()
    {
        if (GameManager.Instance.m_clearScore.active)
        {
            for (int i = 0; i < m_clearButtonList.Count; i++)
            {
                m_clearButtonList[i].enabled = true;
            }

            LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        m_gr = GetComponent<GraphicRaycaster>();
        m_optionButton.onClick.AddListener(OpenOptions);

        m_levelSelectButton.onClick.AddListener(LoadLevelSelect);
        m_backButton.onClick.AddListener(BackToMenuCanvas);
        m_exitButton.onClick.AddListener(ExitToMain);
        m_continueButton.onClick.AddListener(CloseMenu);
        m_optionCanvas.SetActive(false);

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<Button>())
            {
                m_allMenuButtonList.Add(transform.GetChild(i).GetComponent<Button>());
                m_menuButtonList.Add(transform.GetChild(i).GetComponent<Button>());
            }
        }

        for (int i = 0; i < m_optionCanvas.transform.childCount; i++)
        {
            if (m_optionCanvas.transform.GetChild(i).GetComponent<Button>())
            {
                m_optionButtonList.Add(m_optionCanvas.transform.GetChild(i).GetComponent<Button>());
                m_allMenuButtonList.Add(m_optionCanvas.transform.GetChild(i).GetComponent<Button>());
            }
        }

        Button[] ba = GameManager.Instance.m_clearScore.GetComponentsInChildren<Button>();
        foreach (Button i in ba)
        {
            m_clearButtonList.Add(i);
        }

        DisableButtons();
        EnableMenuButtons();

        Button b = transform.GetChild(1).GetComponent<Button>();
        ColorBlock cb = b.colors;
        m_selectedColor = cb.selectedColor;
        m_normalColor = cb.normalColor;
    }

    private void Update()
    {
        m_effectMenu.Rotate(new Vector3(0f, 0f, -1f));
        m_effectOption.Rotate(new Vector3(0f, 0f, -1f));

        if (LevelManager.Instance.m_isShowingMouse)
        {
            LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
            if (LevelManager.Instance.m_eventSystem.IsPointerOverGameObject())
            {
                PointerEventData ped = new PointerEventData(LevelManager.Instance.m_eventSystem);
                ped.position = Input.mousePosition;
                List<RaycastResult> rrl = new List<RaycastResult>();
                m_gr.Raycast(ped, rrl);
                if (rrl.Count > 0)
                {
                    Vector2 lerpPos = m_effectMenu.transform.localPosition;
                    lerpPos.y = Mathf.Lerp(lerpPos.y, rrl[0].gameObject.transform.localPosition.y, Time.deltaTime * 2f);
                    m_effectMenu.localPosition = lerpPos;

                    Vector2 lerpPos2 = m_effectOption.transform.position;
                    lerpPos2.y = Mathf.Lerp(lerpPos2.y, rrl[0].gameObject.transform.position.y, Time.deltaTime * 4f);
                    m_effectOption.position = lerpPos2;

                    //Debug.Log(rrl[0].gameObject.name);
                }
            }
        }
        else
        {
            if (LevelManager.Instance.m_eventSystem.currentSelectedGameObject == null)
            {
                List<Button> buttons = GetActiveButtons();
                LevelManager.Instance.m_eventSystem.SetSelectedGameObject(buttons[0].gameObject);
            }

            Vector2 lerpPos = m_effectMenu.transform.localPosition;
            lerpPos.y = Mathf.Lerp(lerpPos.y, LevelManager.Instance.m_eventSystem.currentSelectedGameObject.transform.localPosition.y, Time.deltaTime * 2f);
            m_effectMenu.localPosition = lerpPos;

            Vector2 lerpPos2 = m_effectOption.transform.localPosition;
            lerpPos2.y = Mathf.Lerp(lerpPos2.y, LevelManager.Instance.m_eventSystem.currentSelectedGameObject.transform.localPosition.y, Time.deltaTime * 4f);
            m_effectOption.localPosition = lerpPos2;
        }
    }

    void OpenOptions()
    {
        DisableButtons();
        

        for (int i = 0; i < m_menuButtonList.Count; i++)
            m_menuButtonList[i].gameObject.GetComponent<Image>().raycastTarget = false;

        m_effectOption.localPosition = new Vector2(m_effectOption.localPosition.x, m_backButton.transform.localPosition.y);

        m_optionCanvas.SetActive(true);
        EnableOptionButtons();
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(m_optionCanvas.transform.GetChild(1).gameObject);

        Debug.Log("Is Clicked");
       
    }

    public void CloseMenu()
    {
        EnableMenuButtons();
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(transform.GetChild(1).gameObject);
        m_effectMenu.localPosition = new Vector2(m_effectMenu.localPosition.x, m_continueButton.transform.localPosition.y);
        GameManager.Instance.m_isGameRunning = true;
        gameObject.SetActive(false);
    }
   
    void BackToMenuCanvas()
    {
        EnableMenuButtons();
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(transform.GetChild(1).gameObject);
        m_effectMenu.localPosition = new Vector2(m_effectMenu.localPosition.x, m_continueButton.transform.localPosition.y);
        m_optionCanvas.SetActive(false);
        Debug.Log("Is Clicked Back");
    }

    void LoadLevelSelect()
    {
        LevelManager.Instance.LoadScene("LevelSelect");
        CloseMenu();
    }
   
    void ExitToMain()
    {
        LevelManager.Instance.LoadScene("MainMenu");
        GameManager.Instance.m_clearScore.SetActive(false);
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
        gameObject.SetActive(false);
    }

    void DisableButtons()
    {
        foreach (Button i in m_allMenuButtonList)
        {
            i.enabled = false;
        }
    }

    void EnableMenuButtons()
    {
        foreach (Button i in m_menuButtonList)
        {
            i.enabled = true;
            i.gameObject.GetComponent<Image>().raycastTarget = true;
        }
    }

    void EnableOptionButtons()
    {
        foreach (Button i in m_optionButtonList)
        {
            i.enabled = true;
        }
    }

    List<Button> GetActiveButtons()
    {
        List<Button> m_activeButtonList = new List<Button>();
        foreach (Button i in m_allMenuButtonList)
        {
            if (i.enabled)
                m_activeButtonList.Add(i);
        }

        return m_activeButtonList;
    }
}
