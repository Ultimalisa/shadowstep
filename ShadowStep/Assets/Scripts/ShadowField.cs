﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowField : MonoBehaviour
{
   
   
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && LightField.m_isStandingInLightField==false && !GameManager.Instance.m_playerIsBeingFollowed)
        {
            collision.transform.GetComponent<PlayerController>().m_allowShadowStepping = true;
           
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.GetComponent<PlayerController>().m_allowShadowStepping = false;
           
        }
    }
}
