﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightField : MonoBehaviour
{
    [SerializeField]
    internal float m_timeToDeath = 2f;
    internal float m_timerForDeath = 0f;
    private float m_currentLerpTime = 0f;
    private float m_lerpDuration = 1f;
    internal float m_minMoveSpeedForPlayer = 3f;
    public static bool m_isStandingInLightField = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_timerForDeath = 0f;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            m_currentLerpTime += Time.deltaTime;
            m_currentLerpTime = Mathf.Clamp(m_currentLerpTime, 0f, m_lerpDuration);

            float perc = m_currentLerpTime / m_lerpDuration;

            m_timerForDeath += Time.deltaTime;

            collision.transform.GetComponent<PlayerController>().m_moveSpeed = Mathf.Lerp(GameManager.Instance.m_originalPlayerMoveSpeed, m_minMoveSpeedForPlayer, perc);
            collision.transform.GetComponent<PlayerController>().m_allowShadowStepping = false;
            m_isStandingInLightField = true;

            if (m_timerForDeath >= m_timeToDeath)
            {
                m_timerForDeath = 0f;
                m_currentLerpTime = 0f;
               
                GameManager.Instance.InititateDeathSequence();
            }
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            collision.transform.GetComponent<PlayerController>().m_moveSpeed = GameManager.Instance.m_originalPlayerMoveSpeed;
        m_timerForDeath = 0f;
        m_currentLerpTime = 0f;
        m_isStandingInLightField = false;
    }

    //private void Update()
    //{
    //    if(m_timerForDeath>= 0.001f)
    //    m_timerForDeath += Time.deltaTime;
    //    if (m_timerForDeath > 3f)
    //    {
    //        m_timerForDeath = 0;
    //        GameManager.Instance.RespawnPlayer();
    //    }
    //}
}
