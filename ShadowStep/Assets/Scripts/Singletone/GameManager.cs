﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    [HideInInspector]
    public GuardManager m_guardManager;
    public AudioManager m_audioManager;
    [SerializeField]
    internal GameObject m_deathScreen;
    public GameObject m_menuCanvas;
    public GameObject m_clearScore;
    public List<Sprite> m_gearSpriteList;
    public Sprite m_crystalOffSprite;

   [HideInInspector]
    public PlayerController m_player;
    [HideInInspector]
    public float m_originalPlayerMoveSpeed;
    public bool m_isGameRunning = true;
    [HideInInspector]
    public bool m_playerIsBeingFollowed = false;

   [HideInInspector]
    public PlayerProgress m_progress;
    private bool m_executeDeathSequence = false;
    public float m_timeToDeath = 1.5f;
    private float m_timer;
    [HideInInspector]
    public int m_score;
    internal AudioSource m_dieAudio;
    [SerializeField]
    internal int m_levelNumber = 5;

    // Start is called before the first frame update
    void Start()
    {
        m_dieAudio = GetComponent<AudioSource>();
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(Instance);
        m_timer = m_timeToDeath;
        m_deathScreen.SetActive(false);
        m_menuCanvas.SetActive(false);
        m_progress = GetComponent<PlayerProgress>();
    }

    private void Update()
    {
       
        if (m_executeDeathSequence)
        {
            m_timer -= Time.deltaTime;

            if (m_timer < 0f)
                RespawnPlayer();
        }
      
       // if (GoalBehavior.Levelend == true)
      //  {
           
      //   PlayerPrefs.SetFloat("playerTime" + LevelManager.Instance.GetCurrentLevelNumber(), m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]);
      //  }

    }

    public void InititateDeathSequence()
    {
      
        m_player.transform.GetChild(1).gameObject.GetComponent<ParticleSystem>().Play();
        m_dieAudio.Play();
        if(m_isGameRunning)
       
       // PlayerPrefs.SetInt("playerDeaths"+ LevelManager.Instance.GetCurrentLevelNumber(), m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber()-1]);
        m_executeDeathSequence = true;
        m_deathScreen.SetActive(true);
        m_isGameRunning = false;
        m_player.GetComponent<LineRenderer>().enabled = false;
        m_player.ResetShadowStep();
    }

    private void RespawnPlayer()
    {
        m_player.transform.localPosition = m_player.m_playerStartPos;
        m_player.transform.localRotation = m_player.m_playerStartRot;
        m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1]++;
        

        m_guardManager.RespawnGuards();

        Reset();
    }

    public void Reset()
    {
        m_executeDeathSequence = false;
        m_timer = m_timeToDeath;
        m_deathScreen.SetActive(false);
        
        m_playerIsBeingFollowed = false;
        m_isGameRunning = true;
       
    }

    public bool IsInitiatingDeathSequence()
    {
        return m_executeDeathSequence;
    }

    public int GetLevelNumber()
    {
        return m_levelNumber;
    }

    public int GetNumberOfLastLevel()
    {
        return m_levelNumber;
    }
}
