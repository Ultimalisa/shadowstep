﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;
using UnityEngine.EventSystems;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance = null;
    private Rewired.Player m_player;
    public bool m_isShowingMouse = true;
   
    public EventSystem m_eventSystem;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(Instance);

        m_player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        if (m_player.GetAxis("UIMouseHorizontal") != 0)
        {
            m_isShowingMouse = true;
            Cursor.visible = true;
        }
        else if (m_player.GetAxis("UIHorizontal") != 0)
        {
            m_isShowingMouse = false;
            Cursor.visible = false;
        }
    }

    public void LoadScene(string sceneName)
    {
        GameManager.Instance.Reset();
        GoalBehavior.m_levelend = false;
        GameManager.Instance.m_audioManager.m_specialEffectsList.Clear();
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
    public void LoadSceneWithIndex(int sceneIndex)
    {
        GoalBehavior.m_levelend = false;
        GameManager.Instance.Reset();
        GameManager.Instance.m_audioManager.m_specialEffectsList.Clear();
        SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
    }
    public int GetCurrentLevelNumber()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        int levelNumber = int.Parse(sceneName.Substring(sceneName.Length - 1, 1));
        return levelNumber;
    }

}
