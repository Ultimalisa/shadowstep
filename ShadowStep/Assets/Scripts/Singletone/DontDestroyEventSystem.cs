﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class DontDestroyEventSystem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (LevelManager.Instance.m_eventSystem == null)
        {
            LevelManager.Instance.m_eventSystem = GetComponent<EventSystem>();
        }

        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
}
