﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionScript : MonoBehaviour
{
    public float delay = 0.05f;
    private string fullText;
    private string currentText = "";
    // Start is called before the first frame update
    private void Start()
    {
        fullText = "But they fear the Light!"+"\n"+"For it posesses the power to banish them back to their rotten plain."+"\n"+"\n"+"The Light brings safety to mankind..."+"\n"+"but what if that last hope were to suddenly disappear?";
    }
    void Update()
    {
        if (TimerForStory.m_timerForStory > 1060)
        {
            if (currentText == "")
                StartCoroutine(ShowText3());
        }


    }
    IEnumerator ShowText3()
    {
        for (int i = 0; i <= fullText.Length; i++)
        {
            currentText = fullText.Substring(0, i);
            this.transform.GetChild(0).GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
    }
   
}
