﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManagerManager : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        if ( GameManager.Instance.m_progress.TimeInLevel[int.Parse(sceneName.Substring(sceneName.Length - 1, 1)) - 1]!= 0f ||
      GameManager.Instance.m_progress.playerDeaths[int.Parse(sceneName.Substring(sceneName.Length - 1, 1)) - 1] != 0 ||
        GameManager.Instance.m_progress.enemiesKilled[int.Parse(sceneName.Substring(sceneName.Length - 1, 1)) - 1] != 0)
        {
           
            GameManager.Instance.m_progress.TimeInLevel[int.Parse(sceneName.Substring(sceneName.Length - 1, 1)) - 1] = 0f;
            GameManager.Instance.m_progress.playerDeaths[int.Parse(sceneName.Substring(sceneName.Length - 1, 1)) - 1] = 0;
            GameManager.Instance.m_progress.enemiesKilled[int.Parse(sceneName.Substring(sceneName.Length - 1, 1)) - 1] = 0;
        }
    }

    public void Exit()
    {
        Application.Quit();
    }


}
