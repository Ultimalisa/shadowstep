﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectScript : MonoBehaviour
{
    public Button Level1;
    public Button Level2;
    public Button Level3;
    public Button Level4;
    // Start is called before the first frame update
    void Start()
    {
        Level1.onClick.AddListener(Load1);
        Level2.onClick.AddListener(Load2);
        Level3.onClick.AddListener(Load3);
        Level4.onClick.AddListener(Load4);
    }

    void Load1()
    {
        LevelManager.Instance.LoadScene("PlaytestLevel1");
    }
    void Load2()
    {
        LevelManager.Instance.LoadScene("Level2");
    }
    void Load3()
    {
        LevelManager.Instance.LoadScene("PlaytestLevel1");
    }
    void Load4()
    {
        LevelManager.Instance.LoadScene("PlaytestLevel1");
    }
   
}
