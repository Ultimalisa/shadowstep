﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public LayerMask m_wallMask;
    public Vector2 m_gridWorldSize;
    private float m_nodeRadius;
    [HideInInspector]
    public float m_distance;

    Node[,] m_grid;
    public List<Node> m_finalPath;

    float m_nodeDiameter;
    int m_gridSizeX, m_gridSizeY;

    // Start is called before the first frame update
    void Start()
    {
        m_nodeRadius = 0.5f;
        m_nodeDiameter = m_nodeRadius * 2f;
        m_gridSizeX = Mathf.RoundToInt(m_gridWorldSize.x / m_nodeDiameter); // node width
        m_gridSizeY = Mathf.RoundToInt(m_gridWorldSize.y / m_nodeDiameter); // node height

        CreateGrid();
    }

    void CreateGrid()
    {
        m_grid = new Node[m_gridSizeX, m_gridSizeY];
        Vector3 bottomLeft = transform.position - Vector3.right * m_gridWorldSize.x / 2f - Vector3.up * m_gridWorldSize.y / 2f;

        for (int y = 0; y < m_gridSizeY; y++)
        {
            for (int x = 0; x < m_gridSizeX; x++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * m_nodeDiameter + m_nodeRadius) + Vector3.up * (y * m_nodeDiameter + m_nodeRadius); // middle point of node
                bool wall = false;

                if (Physics2D.OverlapCircle(worldPoint, m_nodeRadius, m_wallMask))
                    wall = true;

                m_grid[x, y] = new Node(wall, worldPoint, x, y);

            }
        }
    }

    public Node NodeFromWorldPosition(Vector3 worldPos)
    {
        //float xPoint = ((worldPos.x + m_gridWorldSize.x / 2) / m_gridWorldSize.x); //gets procentual position of grid point on x
        //float yPoint = ((worldPos.y + m_gridWorldSize.y / 2) / m_gridWorldSize.y); //gets procentual position of grid point on y

        //xPoint = Mathf.Clamp01(xPoint);
        //yPoint = Mathf.Clamp01(yPoint);

        //int x = Mathf.RoundToInt((m_gridSizeX - 1) * xPoint);
        //int y = Mathf.RoundToInt((m_gridSizeY - 1) * yPoint);

        Vector3 bottomLeft = transform.position - Vector3.right * m_gridWorldSize.x / 2f - Vector3.up * m_gridWorldSize.y / 2f;
        Vector2 relativeWorldPos = worldPos - bottomLeft;

        int x = (int)Mathf.Clamp(Mathf.FloorToInt(relativeWorldPos.x / m_nodeDiameter), 0, m_gridWorldSize.x);
        int y = (int)Mathf.Clamp(Mathf.FloorToInt(relativeWorldPos.y / m_nodeDiameter), 0, m_gridWorldSize.y);

        return m_grid[x, y];
    }

    public List<Node> GetNeighboringNodes(Node node)
    {
        List<Node> NeighboringNodes = new List<Node>();

        for(int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                // skip the node in the middle as it is the node that was passed in
                if (x == 0 && y == 0)
                {
                    continue;
                }

                int xCheck;
                int yCheck;

                xCheck = node.m_gridX + x;
                yCheck = node.m_gridY + y;

                if (xCheck >= 0 && xCheck < m_gridSizeX && yCheck >= 0 && yCheck < m_gridSizeY)
                    NeighboringNodes.Add(m_grid[xCheck, yCheck]);
            }
        }

        

        ////Right Side
        //xCheck = node.m_gridX + 1;
        //yCheck = node.m_gridY;
        //if(xCheck >= 0 && xCheck < m_gridSizeX)
        //{
        //    if (yCheck >= 0 && yCheck < m_gridSizeY)
        //        NeighboringNodes.Add(m_grid[xCheck, yCheck]);
        //}

        ////Left Side
        //xCheck = node.m_gridX - 1;
        //yCheck = node.m_gridY;
        //if (xCheck >= 0 && xCheck < m_gridSizeX)
        //{
        //    if (yCheck >= 0 && yCheck < m_gridSizeY)
        //        NeighboringNodes.Add(m_grid[xCheck, yCheck]);
        //}

        ////Up Side
        //xCheck = node.m_gridX;
        //yCheck = node.m_gridY + 1;
        //if (xCheck >= 0 && xCheck < m_gridSizeX)
        //{
        //    if (yCheck >= 0 && yCheck < m_gridSizeY)
        //        NeighboringNodes.Add(m_grid[xCheck, yCheck]);
        //}

        ////Down Side
        //xCheck = node.m_gridX;
        //yCheck = node.m_gridY - 1;
        //if (xCheck >= 0 && xCheck < m_gridSizeX)
        //{
        //    if (yCheck >= 0 && yCheck < m_gridSizeY)
        //        NeighboringNodes.Add(m_grid[xCheck, yCheck]);
        //}

        return NeighboringNodes;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(m_gridWorldSize.x, m_gridWorldSize.y, 1));

        if (m_grid != null)
        {
            foreach (Node node in m_grid)
            {
                if (node.m_isWall)
                    Gizmos.color = Color.yellow;
                else
                    Gizmos.color = Color.white;

                if (m_finalPath != null)
                {
                    if (m_finalPath.Contains(node))
                        Gizmos.color = Color.red;
                }


                Gizmos.DrawCube(node.m_worldPosition, Vector3.one * (m_nodeDiameter - m_distance));
            }
        }
    }
#endif

    // Pathfinding
    public List<Node> FindPath(Vector3 startPos, Vector3 targetPos)
    {
        Node StartNode = NodeFromWorldPosition(startPos);
        Node TargetNode = NodeFromWorldPosition(targetPos);

        //Debug.DrawLine(StartNode.m_worldPosition, TargetNode.m_worldPosition, Color.cyan);

        if(StartNode.m_isWall)
        {
            List<Node> neighbors = GetNeighboringNodes(StartNode);
            for (int i = 0; i < neighbors.Count; i++)
            {
                if(neighbors[i].m_isWall == false)
                {
                    StartNode = neighbors[i];
                    break;
                }
            }
        }

        List<Node> OpenList = new List<Node>();
        HashSet<Node> ClosedList = new HashSet<Node>();

        OpenList.Add(StartNode);

        while (OpenList.Count > 0)
        {
            Node CurrentNode = OpenList[0];
            for (int i = 1; i < OpenList.Count; i++)
            {
                if (OpenList[i].m_fCost <= CurrentNode.m_fCost && OpenList[i].m_hCost < CurrentNode.m_hCost)
                {
                    CurrentNode = OpenList[i];
                }
            }

            OpenList.Remove(CurrentNode);
            ClosedList.Add(CurrentNode);

            if (CurrentNode == TargetNode)
            {
                return GetFinalPath(StartNode, TargetNode);
            }

            foreach (Node NeighborNode in GetNeighboringNodes(CurrentNode))
            {
                if (NeighborNode.m_isWall || ClosedList.Contains(NeighborNode))
                    continue;

                int MoveCost = CurrentNode.m_gCost + GetManhattenDistance(CurrentNode, NeighborNode);

                if (MoveCost < NeighborNode.m_gCost || !OpenList.Contains(NeighborNode))
                {
                    NeighborNode.m_gCost = MoveCost;
                    NeighborNode.m_hCost = GetManhattenDistance(NeighborNode, TargetNode);
                    NeighborNode.m_parent = CurrentNode;

                    if (!OpenList.Contains(NeighborNode))
                        OpenList.Add(NeighborNode);
                }
            }
        }

        return new List<Node>();
    }

    public List<Node> GetFinalPath(Node startNode, Node endNode)
    {
        List<Node> FinalPath = new List<Node>();
        Node CurrentNode = endNode;

        while (CurrentNode != startNode)
        {
            FinalPath.Add(CurrentNode);
            CurrentNode = CurrentNode.m_parent;
        }

        //FinalPath.Reverse();

        return FinalPath;
    }

    public int GetManhattenDistance(Node nodeA, Node nodeB)
    {
        int iX = Mathf.Abs(nodeA.m_gridX - nodeB.m_gridX);
        int iY = Mathf.Abs(nodeA.m_gridY - nodeB.m_gridY);

        return iX + iY;
    }
}
