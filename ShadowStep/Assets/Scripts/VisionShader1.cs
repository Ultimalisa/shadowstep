﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VisionShader1 : MonoBehaviour
{
    public Material mat;

    // Takes the RenderTexture of the source and renders it to the destination
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, mat);
    }
}
