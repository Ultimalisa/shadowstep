﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Story : MonoBehaviour
{

    public float delay = 0.05f;
    private string fullText;
    private string currentText = "";

    private void Start()
    {
       
        fullText = "In Cydonia the long night has fallen." + "\n" + "Sol has hidden his face for another year closing the gap between realms." + "\n" + "\n" + "Yet Man has learned to keep the terrors of the night away from their city." + "\n" + "Mighty generators bring light into the darkness.";
        StartCoroutine(ShowText());

    }
    // Update is called once per frame
    void Update()
    {
       
        if (TimerForStory.m_timerForStory > 590)
            gameObject.SetActive(false);
    }

    IEnumerator ShowText()
    {
        for(int i=0;i<=fullText.Length;i++)
        {
            currentText = fullText.Substring(0, i);
            this.transform.GetChild(0).GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
    }
}
