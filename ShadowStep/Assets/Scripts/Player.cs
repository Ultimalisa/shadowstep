﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    internal bool m_isShadowStepping = false;

    private Rigidbody2D rb;
    public float m_speed = 5.0f;
   
    public float m_dashspeed = 20.0f;
    public Text m_gtext;
    private Vector2 m_moveVelocity;
    private Vector2 m_moveVelocity2;
    public GameObject m_Arrow;
    private float m_timer = 0;
    private float m_curRotAngle;
    public float m_acceleration=0.001f;
    public float m_dashTime = 0.3f;
    private float m_cooldown = 0;
    public float m_cooldownMax = 3;
    public bool m_useCooldown = false;
    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        m_gtext.enabled = false;
        m_Arrow.SetActive(true);
       
        //transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        m_timer = Mathf.Clamp(m_timer - Time.deltaTime, -1f, m_dashTime);
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal")*m_acceleration, Input.GetAxisRaw("Vertical")*m_acceleration);
        //faceMouse();
        faceJoystick();

        if (Input.GetAxisRaw("Horizontal") == 0f && Input.GetAxisRaw("Vertical") == 0f)
            m_acceleration = 0.001f;

        if (m_acceleration < 1f && (Input.GetAxisRaw("Horizontal") != 0f || Input.GetAxisRaw("Vertical") != 0f))
            m_acceleration += 0.01f;
        //Debug.Log(m_acceleration);
        //transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + mousex, transform.localEulerAngles.x + mousey);

        /* Quaternion rotation = transform.rotation;
         curRotAngle = rotation.eulerAngles.y;
         curRotAngle = Mathf.Atan2(mousex, mousey);
         Vector3 rotAngle = rotation.eulerAngles;
         rotAngle.y = curRotAngle;
         rotation.eulerAngles = rotAngle;
         transform.rotation = Camera.main.ScreenToWorldPoint(rotAngle);*/

        m_moveVelocity = moveInput * m_speed;
       
        if (transform.position.x >=7f && transform.position.y >= 3f)
        {
            m_gtext.enabled = false;
           
        }

        if (m_useCooldown)
        {
            m_cooldown -= Time.deltaTime;
            if (m_cooldown > 0)
            {
                int CooldownInt = (int)m_cooldown + 1;
                string cd = CooldownInt.ToString();
                m_gtext.text = "cooldown: " + cd;
            }
            else
                m_gtext.enabled = false;
        }


        if ((Input.GetMouseButtonDown(0) || Input.GetButtonDown("ControllerB")) )
        {
            if (!m_useCooldown)
            {
                m_timer = m_dashTime;
                m_isShadowStepping = true;
            }

            else
            {
                if (m_cooldown < 0)
                {
                    m_timer = m_dashTime;
                    m_cooldown = m_cooldownMax;
                    m_gtext.enabled = true;
                    m_isShadowStepping = true;
                }
            }

        }
        if (m_timer > 0)
        {
            transform.Translate(0f, m_dashspeed * 1.5f * Time.deltaTime, 0f);
        }
        else
            m_isShadowStepping = false;

        
        
    }
       
    void faceMouse()
    {
        float mousex = Input.GetAxisRaw("Mouse X");
        float mousey = Input.GetAxisRaw("Mouse Y");

        Vector3 mouseposition = Input.mousePosition;
        mouseposition = Camera.main.ScreenToWorldPoint(mouseposition);
        Vector2 direction = new Vector2(mouseposition.x - transform.position.x, mouseposition.y - transform.position.y);
        transform.up = direction;
    }      
    
    void faceJoystick()
    {
        float xRotation = -Input.GetAxisRaw("Horizontal");
        float yRotation = -Input.GetAxisRaw("Vertical");
        Vector3 stickPos = new Vector3(xRotation, yRotation, 0f);

        //stickPos = Camera.main.ScreenToWorldPoint(stickPos);
        //Vector2 direction = new Vector2(stickPos.x - transform.position.x, stickPos.y - transform.position.y);
        //transform.up = direction.normalized;

        
            Quaternion playerRot = transform.rotation;
            float curRotAngleY = playerRot.eulerAngles.y;
            curRotAngleY = Mathf.Atan2(stickPos.y, stickPos.x);
            Vector3 rotAngles = playerRot.eulerAngles;
            rotAngles.z = curRotAngleY * Mathf.Rad2Deg + 90f;
            playerRot.eulerAngles = rotAngles;
            transform.rotation = playerRot;
        

    }
    void FixedUpdate()
    {
        
        rb.MovePosition(rb.position + m_moveVelocity * Time.deltaTime);


    }
}
