﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GuardManager : MonoBehaviour
{
    internal List<SGuard> m_guardList = new List<SGuard>();

    void Start()
    {
        GameManager.Instance.m_guardManager = this;


        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(transform.GetChild(0).GetChild(0).gameObject.GetComponents<AudioSource>()[0]);
        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(transform.GetChild(0).GetChild(0).gameObject.GetComponents<AudioSource>()[1]);
        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(transform.GetChild(0).GetChild(0).gameObject.GetComponents<AudioSource>()[2]);
        for (int i = 0; i < transform.GetChild(0).transform.childCount; i++)
        {
            if (transform.GetChild(0).transform.GetChild(i).gameObject.activeSelf)
            {
                m_guardList.Add(transform.GetChild(0).transform.GetChild(i).transform.GetComponent<GuardLogic>().m_guard);
            }
        }
    }

    public void RespawnGuards()
    {
        if (m_guardList.Count != 0)
        {
            for (int i = 0; i < m_guardList.Count; i++)
            {
                m_guardList[i].m_gameObject.transform.localPosition = m_guardList[i].m_startPos;
                m_guardList[i].m_gameObject.transform.localRotation = m_guardList[i].m_startRot;
                m_guardList[i].m_guardLogic.ResetValues();
                m_guardList[i].m_gameObject.SetActive(true);
            }
        }
    }

#if UNITY_EDITOR
    public void CreateGuard(GameObject guardPrefab)
    {
        //TODO: make another Editor Script
        GameObject go = PrefabUtility.InstantiatePrefab(guardPrefab, transform.GetChild(0).transform) as GameObject; //(guardPrefab, new Vector3(5, 5, 0), Quaternion.identity, transform.GetChild(0).transform);
        go.transform.rotation = Quaternion.identity;
        go.transform.localPosition = new Vector3(0, 0, 0);
    }
#endif
    //TODO guardsNotInRange -> Do not activate / Renderprobleme beheben
}
