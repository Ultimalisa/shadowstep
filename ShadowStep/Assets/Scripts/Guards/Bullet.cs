﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float m_lifeTime = 0f;
    private ParticleSystem m_ps;
    private SpriteRenderer m_sr;
    private BoxCollider2D m_bc;
    private bool m_checkForExplosion;


    private void Update()
    {
        if (GameManager.Instance.m_isGameRunning)
        {
            if (m_lifeTime > 0)
                m_lifeTime -= Time.deltaTime;

            if (m_lifeTime < 0)
                Destroy(gameObject);
        }
        else
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;

        if(m_checkForExplosion)
        {
            if (m_ps.isStopped)
                Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.gameObject.GetComponent<PlayerController>())
        {
            // play ps
            // Destroy GO at the end of ps.anim
            m_sr.enabled = false;
            m_bc.enabled = false;
            m_ps.Play();
            m_checkForExplosion = true;
            GameManager.Instance.InititateDeathSequence();
        }
        else
        {
            // let bullet explode?
            m_sr.enabled = false;
            m_bc.enabled = false;
            m_ps.Play();
            m_checkForExplosion = true;
        }
    }

    public void EnableBullet(float lt)
    {
        m_lifeTime = lt;
        m_sr = GetComponent<SpriteRenderer>();
        m_sr.enabled = true;
        m_bc = GetComponent<BoxCollider2D>();
        m_bc.enabled = true;
        m_ps = GetComponent<ParticleSystem>();
        m_checkForExplosion = false;
    }
}
