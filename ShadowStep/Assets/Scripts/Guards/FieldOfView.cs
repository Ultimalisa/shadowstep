﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour
{

    public float m_viewRadius;
    [Range(0, 360)]
    public float m_viewAngle;
    [SerializeField]
    internal float m_distOfDeath;
    [SerializeField]
    internal float m_renderingDistance = 150f;

    public LayerMask m_targetMask;
    public LayerMask m_obstacleMask;

    [HideInInspector]
    public Transform m_visibleTarget;
    [SerializeField]
    internal float m_reactionDelayMax = 0.2f;
    private float m_delayTimer;

    public float m_meshResolution;

    public MeshFilter m_viewMeshFilter;
    private Mesh m_viewMesh;
    public AudioSource m_hitSound;
    private GuardLogic m_gl;

    void Start()
    {
       
        m_viewMesh = new Mesh();
        m_viewMesh.name = "View Mesh";
        m_viewMeshFilter.mesh = m_viewMesh;

        if (m_distOfDeath > m_viewRadius)
            m_distOfDeath = m_viewRadius;

        m_gl = GetComponent<GuardLogic>();

        m_delayTimer = m_reactionDelayMax;
    }

    private void Update()
    {
        if (m_gl.GetGuardState() != EGuardState.GS_IsDying)
            FindVisibleTargets();
    }

    private void LateUpdate()
    {
        if(new Vector2(transform.position.x - GameManager.Instance.m_player.transform.position.x, transform.position.y - GameManager.Instance.m_player.transform.position.y).sqrMagnitude < m_renderingDistance)
            DrawFieldOfView();
    }

    public struct ViewCastInfo
    {
        public bool bHit;
        public Vector3 pt;
        public float dst;
        public float ang;

        public ViewCastInfo(bool hit, Vector3 point, float distance, float angle)
        {
            bHit = hit;
            pt = point;
            dst = distance;
            ang = angle;
        }
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 Dir = DirFromAngle(globalAngle, false);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Dir, m_viewRadius, m_obstacleMask);
 
        if (hit.collider != null)
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        else
            return new ViewCastInfo(false, transform.position + Dir * m_viewRadius, hit.distance, globalAngle);
    }

    void FindVisibleTargets()
    {
        Collider2D TargetInViewRadius = Physics2D.OverlapCircle(transform.position, m_viewRadius, m_targetMask);
       
        if (TargetInViewRadius != null)
        {
            // reaction time delay
            m_delayTimer -= Time.deltaTime;

            Transform Target = TargetInViewRadius.transform;
            Vector2 DirToTarget = new Vector2(Target.position.x - transform.position.x, Target.position.y - transform.position.y).normalized;
            float angle = Vector2.Angle(transform.up, DirToTarget);
            if (angle < m_viewAngle / 2)
            {
                float DstToTarget = Vector2.Distance(transform.position, Target.position);

                if (!Physics2D.Raycast(transform.position, DirToTarget, DstToTarget, m_obstacleMask))
                {
                    m_visibleTarget = TargetInViewRadius.transform;

                    if (!m_gl.m_isShooter && m_delayTimer < 0f)
                    {
                        if (DstToTarget <= m_distOfDeath && !m_visibleTarget.gameObject.GetComponent<PlayerController>().m_isShadowStepping)
                        {
                            m_hitSound.Play();
                            m_gl.animator.SetBool("IsPunching", true);
                            m_visibleTarget = null;

                            GameManager.Instance.InititateDeathSequence();

                            //Debug.Log("Killing Player");
                        }
                    }
                }
                else
                {
                    m_visibleTarget = null;
                }

            }
            else
            {
                m_visibleTarget = null;
            }
            
        }
        else
        {
            m_visibleTarget = null;
            ResetDelayTimer();
        }
        if (TargetInViewRadius == null || m_visibleTarget == null)
        {
            m_gl.animator.SetBool("IsPunching", false);
        }
    }

    void DrawFieldOfView()
    {
        int StepCount = Mathf.RoundToInt(m_viewAngle * m_meshResolution);
        float StepAngleSize = m_viewAngle / StepCount;
        List<Vector3> ViewPoints = new List<Vector3>();

        for (int i = 0; i <= StepCount; i++)
        {
            float Angle = -transform.eulerAngles.z - m_viewAngle / 2f + StepAngleSize * i;
            //Debug.DrawLine(transform.position, transform.position + DirFromAngle(Angle, false) * m_viewRadius, Color.red);
            ViewCastInfo NewViewCast = ViewCast(Angle);
            ViewPoints.Add(NewViewCast.pt);
        }

        int VertexCount = ViewPoints.Count + 1;
        Vector3[] Vertices = new Vector3[VertexCount];
        int[] Triangles = new int[(VertexCount - 2) * 3];

        Vertices[0] = Vector3.zero;
        for (int i = 0; i < VertexCount - 1; i++)
        {
            Vertices[i + 1] = transform.InverseTransformPoint(ViewPoints[i]);

            if (i < VertexCount - 2)
            {
                Triangles[i * 3] = 0;
                Triangles[i * 3 + 1] = i + 1;
                Triangles[i * 3 + 2] = i + 2;
            }
        }

        m_viewMesh.Clear();
        m_viewMesh.vertices = Vertices;
        m_viewMesh.triangles = Triangles;
        m_viewMesh.RecalculateNormals();
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsLocal)
    {
        if (angleIsLocal)
        {
            angleInDegrees -= transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), 0);
    }

    public void ResetDelayTimer()
    {
        m_delayTimer = m_reactionDelayMax;
    }
}