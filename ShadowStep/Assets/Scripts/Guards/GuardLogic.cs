﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Unity.Collections;

public enum EGuardState
{
    GS_IsMoving,
    GS_IsTurning,
    GS_IsFollowingTarget,
    GS_IsAiming,
    GS_IsLookingAround,
    GS_IsReturningToRoutine,
    GS_IsStanding,
    GS_IsRotatingToStartPos,
    GS_IsDying
};

public enum EGuardType
{
    GT_Routine,
    GT_Cycle,
    GT_Static
};

[System.Serializable]
public struct SGuard
{
    [HideInInspector]
    public Vector3 m_startPos;
    [HideInInspector]
    public Quaternion m_startRot;
    [HideInInspector]
    public GameObject m_gameObject;
    public List<GameObject> m_checkPointList;

    [HideInInspector]
    public GuardLogic m_guardLogic;
    [HideInInspector]
    public int m_currentCheckPointNumber;
}

public class GuardLogic : MonoBehaviour
{
    
    private Vector3 m_startPosGL;
    [SerializeField]
    internal float m_moveSpeed;
    [SerializeField]
    internal float m_followSpeed;
    [SerializeField]
    internal float m_rotationSpeed = 6f;
    private float m_currentLerpTime = 0;
    [SerializeField]
    internal float m_lerpDuration = 1;
    private ParticleSystem m_ps;
    [SerializeField]
    internal MeshRenderer m_meshRenderer;

    internal Vector2 m_dirToNextCP;
    internal Quaternion m_startRotGL;

    private Vector3 m_lastPosition;

    private EGuardState m_eGuardState;

    [SerializeField]
    internal EGuardType m_eGuardType = EGuardType.GT_Routine;

    private FieldOfView m_fieldOfView;

    public SGuard m_guard;
    public Animator animator;
    private int m_dir = 1;

    private Grid m_grid = null;
    private List<Node> m_pathBackToRoutine = new List<Node>();
   
    [SerializeField]
    internal GameObject m_CP;
    public AudioSource m_bulletClip;
    internal AudioSource stepping;
    private float m_timer;
    private float m_firingTimer;
    [SerializeField]
    internal float m_firingRateOfShooter = 0.7f;
    [SerializeField]
    internal float m_bulletOffset = 2f;
    [SerializeField]
    internal float m_bulletSpeed = 6f;
    [SerializeField]
    internal float m_bulletLifetime = 0.7f;

    [SerializeField]
    internal bool m_isShooter = false;
    private bool m_switched = false;

    private float m_lookAroundTimer;

    //public GuardController(SGuard sGuard)
    //{
    //    m_guard = sGuard;
    //}

    public void Start()
    {
        animator = GetComponent<Animator>();
        m_grid = FindObjectOfType<Grid>();
        stepping = GetComponent<AudioSource>();
        if (m_isShooter)
        {
            animator.SetBool("IsShooter", true);
        }
        // else animator.SetBool("IsShooter", false);
        // Set Values for Guard Logic
        if (m_eGuardType != EGuardType.GT_Static)
        {
            Vector2 dirToFirstCP = (m_guard.m_checkPointList[0].transform.localPosition - transform.localPosition).normalized;

            Quaternion PlayerRot = transform.localRotation;
            Vector3 RotAngles = PlayerRot.eulerAngles;
            RotAngles.z = (PlayerRot * Quaternion.FromToRotation(transform.up, dirToFirstCP).normalized).normalized.eulerAngles.z;
            PlayerRot.eulerAngles = RotAngles;
            transform.localRotation = PlayerRot;

            m_eGuardState = EGuardState.GS_IsMoving;
        }
        else
            m_eGuardState = EGuardState.GS_IsStanding;

        m_startPosGL = transform.position;

        // Set Values in SGuard
        m_guard.m_gameObject = this.gameObject;
        m_guard.m_startPos = transform.localPosition;
        m_guard.m_currentCheckPointNumber = 1;
        m_guard.m_startRot = transform.localRotation;

        
        m_fieldOfView = m_guard.m_gameObject.GetComponent<FieldOfView>();
        m_lastPosition = new Vector3(0f, 0f, -1f);

        // Assign Guard Logic to SGuard
        m_guard.m_guardLogic = this;

        // Add Startpoint of Guard as new Checkpoint
        //GameObject cp = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/CheckPointGuard.prefab", typeof(GameObject));
        GameObject go = Instantiate(m_CP, transform.position, Quaternion.identity);
        m_guard.m_checkPointList.Insert(0, go);

        go.transform.parent = this.transform.parent.transform.parent.GetChild(1);

        m_ps = GetComponent<ParticleSystem>();

        //only hide checkpoints when we are playing in game mode
        if (Application.isPlaying && !Application.isEditor)
        {
            for (int i = 0; i < m_guard.m_checkPointList.Count; i++)
            {
                m_guard.m_checkPointList[i].GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }

    private void Update()
    {
        if (animator.GetBool("IsWalking") == true)
        {
            if (!m_switched)
            {
                stepping.Play();
                m_switched = true;
            }

        }
        else m_switched = false;
        if(m_eGuardState == EGuardState.GS_IsMoving || m_eGuardState == EGuardState.GS_IsFollowingTarget || m_eGuardState == EGuardState.GS_IsReturningToRoutine)
        {
            animator.SetBool("IsWalking", true);
           
        }else 
        {
            animator.SetBool("IsWalking", false);
        }
        if (GameManager.Instance.m_isGameRunning)
        {
            float frameTime = Time.smoothDeltaTime;

            // increment timer once per frame
            m_currentLerpTime += frameTime * m_moveSpeed;
            m_currentLerpTime = Mathf.Clamp(m_currentLerpTime, 0f, m_lerpDuration);

            float percentage = m_currentLerpTime * 20f / m_lerpDuration;

            // lerping from 0 to 100 by the time of m_lerpDuration with the pitch from below
            float t = m_currentLerpTime / m_lerpDuration;
            t = t * t * (3f - 2f * t);

            m_timer = Mathf.Clamp(m_timer - Time.deltaTime, -1f, 1f);
            //Debug.Log("m_lerpDuration:" + m_lerpDuration);

            if (m_fieldOfView.m_visibleTarget != null && m_eGuardState != EGuardState.GS_IsFollowingTarget && m_eGuardState != EGuardState.GS_IsAiming && m_eGuardState != EGuardState.GS_IsDying)
            {
                if (m_isShooter)
                {
                    m_eGuardState = EGuardState.GS_IsAiming;
                }
                   
                else
                    m_eGuardState = EGuardState.GS_IsFollowingTarget;

                GameManager.Instance.m_playerIsBeingFollowed = true;
            }

            switch (m_eGuardState)
            {
                case EGuardState.GS_IsMoving:
                    transform.position = Vector2.Lerp(m_startPosGL, m_guard.m_checkPointList[m_guard.m_currentCheckPointNumber].transform.position, t);
                   
                  
                    if (t >= 1)
                    {
                        // Reset Stats
                        m_currentLerpTime = 0f;

                        m_eGuardState = EGuardState.GS_IsTurning;

                        //change current Checkpoint to new start pos and set the next goalRot angle
                        //m_startPosGL = m_guard.m_checkPointList[m_guard.m_currentCheckPointNumber].transform.position;
                        m_startPosGL = transform.position;

                        if (m_eGuardType == EGuardType.GT_Routine)
                        {
                            // hit last or first element of CPL ? turn the direction
                            if (m_guard.m_currentCheckPointNumber == (m_guard.m_checkPointList.Count - 1) || m_guard.m_currentCheckPointNumber == 0)
                                m_dir *= -1;


                            m_guard.m_currentCheckPointNumber += m_dir;
                        }
                        if (m_eGuardType == EGuardType.GT_Cycle)
                        {
                            // hit last element of CPL? Set first CP as current CP
                            if (m_guard.m_currentCheckPointNumber == (m_guard.m_checkPointList.Count - 1))
                                m_guard.m_currentCheckPointNumber = 0;
                            else
                                m_guard.m_currentCheckPointNumber++;
                        }

                        Vector2 dirToNextTarget = (m_guard.m_checkPointList[m_guard.m_currentCheckPointNumber].transform.localPosition - transform.localPosition).normalized;
                        m_dirToNextCP = dirToNextTarget;

                        //m_startRotGL = transform.localRotation;
                    }
                    break;

                case EGuardState.GS_IsTurning:
                    //animator.SetBool("IsWalking", false);
                    animator.StopPlayback();
                    Matrix4x4 PlayerTrans = transform.localToWorldMatrix;
                    Quaternion PlayerRot = transform.localRotation;
                    Vector3 angles = PlayerRot.eulerAngles;
                    angles.x = 0f;
                    angles.y = 0f;
                    angles.z = Quaternion.Lerp(PlayerRot, PlayerRot * Quaternion.FromToRotation(transform.up, m_dirToNextCP).normalized, Time.deltaTime * m_rotationSpeed).normalized.eulerAngles.z;
                    PlayerRot.eulerAngles = angles;
                    transform.localRotation = PlayerRot;

                    //transform.localRotation = Quaternion.Lerp(m_startRotGL, transform.localRotation * Quaternion.FromToRotation(transform.up, m_dirToNextCP).normalized, t);

                    if (PlayerRot.eulerAngles.z <= (PlayerRot * Quaternion.FromToRotation(transform.up, m_dirToNextCP)).normalized.eulerAngles.z + 0.1f && PlayerRot.eulerAngles.z >= (PlayerRot * Quaternion.FromToRotation(transform.up, m_dirToNextCP)).normalized.eulerAngles.z - 0.1f)
                    {
                        // Reset stats
                        m_currentLerpTime = 0f;
                        m_eGuardState = EGuardState.GS_IsMoving;
                    }
                    break;

                case EGuardState.GS_IsFollowingTarget:
                   
                    // has lost target?
                    if (m_fieldOfView.m_visibleTarget == null)
                    {
                        animator.SetBool("IsPunching", false);
                        // Reset Stats
                        //m_currentLerpTime = 0f;
                        //m_startPosGL = transform.position;
                        m_timer = 0.5f;
                        m_eGuardState = EGuardState.GS_IsLookingAround;
                        GameManager.Instance.m_playerIsBeingFollowed = false;
                    }
                    else
                    {
                        m_pathBackToRoutine.Clear();

                        // remember current position and angle
                        if (m_lastPosition.z <= -1f)
                        {
                            m_lastPosition = transform.position;
                        }

                        // Rotate to target
                        PlayerRot = transform.localRotation;
                        
                        PlayerTrans = Matrix4x4.Rotate(Quaternion.Lerp(PlayerRot, PlayerRot * Quaternion.FromToRotation(transform.up, new Vector2(m_fieldOfView.m_visibleTarget.position.x - transform.position.x, m_fieldOfView.m_visibleTarget.position.y - transform.position.y).normalized).normalized, Time.smoothDeltaTime * m_rotationSpeed).normalized);
                        transform.localRotation = PlayerTrans.rotation;

                        // follow target
                        transform.position = Vector2.Lerp(transform.position, m_fieldOfView.m_visibleTarget.transform.position, Time.deltaTime * m_followSpeed);
                    }

                    break;

                case EGuardState.GS_IsAiming:
                   
                    // has lost target?
                    if (m_fieldOfView.m_visibleTarget == null)
                    {
                        animator.SetBool("IsShooting", false);
                        // Reset Stats
                        //m_currentLerpTime = 0f;
                        //m_startPosGL = transform.position;
                        m_timer = 0.5f;
                        m_eGuardState = EGuardState.GS_IsLookingAround;
                        GameManager.Instance.m_playerIsBeingFollowed = false;
                    }
                    else
                    {
                        m_firingTimer = Mathf.Clamp(m_firingTimer - Time.deltaTime, -1f, m_firingRateOfShooter);

                        // Rotate to target
                        PlayerRot = transform.localRotation;

                        PlayerTrans = Matrix4x4.Rotate(Quaternion.Lerp(PlayerRot, PlayerRot * Quaternion.FromToRotation(transform.up, new Vector2(m_fieldOfView.m_visibleTarget.position.x - transform.position.x, m_fieldOfView.m_visibleTarget.position.y - transform.position.y).normalized).normalized, Time.smoothDeltaTime * m_rotationSpeed).normalized);
                        transform.localRotation = PlayerTrans.rotation;

                        if(m_firingTimer < 0)
                        {
                            animator.SetBool("IsShooting", true);
                            // Shoot!
                            m_bulletClip.Play();
                            GameObject go = GameObject.Instantiate<GameObject>(transform.GetChild(2).gameObject, transform.position + transform.up * m_bulletOffset, transform.rotation);
                            go.GetComponent<Rigidbody2D>().velocity = new Vector2(m_fieldOfView.m_visibleTarget.position.x - transform.position.x, m_fieldOfView.m_visibleTarget.position.y - transform.position.y).normalized * m_bulletSpeed;
                            go.GetComponent<Bullet>().EnableBullet(m_bulletLifetime);

                            m_firingTimer = m_firingRateOfShooter;
                        }
                    }

                    break;

                case EGuardState.GS_IsLookingAround:
                    // animator.SetBool("IsWalking", false);
                    animator.StopPlayback();

                    m_lookAroundTimer -= Time.deltaTime;

                    if (m_lookAroundTimer > -0.8f)
                    {
                        transform.Rotate(new Vector3(0f, 0f, -1f));
                    }
                    if(m_lookAroundTimer <= -0.8f && m_lookAroundTimer > -1.2f)
                    {
                        transform.Rotate(new Vector3(0f, 0f, 2f));
                    }
                    if(m_lookAroundTimer <= -1.2f && m_lookAroundTimer > -2f)
                    {
                        transform.Rotate(new Vector3(0f, 0f, 1f));
                    }
                    if(m_lookAroundTimer <= -2f && m_lookAroundTimer > -2.4f)
                    {
                        transform.Rotate(new Vector3(0f, 0f, -2f));
                    }

                    // Reset
                    if(m_lookAroundTimer <= -2.4f)
                    {
                        m_currentLerpTime = 0f;
                        m_startPosGL = transform.position;
                        m_lookAroundTimer = 0;

                        if (m_eGuardType == EGuardType.GT_Static)
                            m_eGuardState = EGuardState.GS_IsRotatingToStartPos;
                        else
                            m_eGuardState = EGuardState.GS_IsReturningToRoutine;
                    }

                    break;

                case EGuardState.GS_IsReturningToRoutine:

                    //Debug.DrawLine(transform.position, m_lastPosition, Color.cyan);

                    if (m_pathBackToRoutine.Count == 0)
                    {
                        m_pathBackToRoutine = m_grid.FindPath(transform.position, m_lastPosition);
                    }

                    if (m_pathBackToRoutine.Count > 0)
                    {
                        PlayerRot = transform.localRotation;
                        Matrix4x4 playerTrans = transform.localToWorldMatrix;
                        playerTrans = Matrix4x4.Rotate(Quaternion.Lerp(PlayerRot, PlayerRot * Quaternion.FromToRotation(transform.up, new Vector2(m_pathBackToRoutine[m_pathBackToRoutine.Count - 1].m_worldPosition.x - transform.position.x, m_pathBackToRoutine[m_pathBackToRoutine.Count - 1].m_worldPosition.y - transform.position.y).normalized).normalized, Time.smoothDeltaTime * m_rotationSpeed));
                        transform.localRotation = playerTrans.rotation;
                       
                        transform.position = Vector2.Lerp(m_startPosGL, m_pathBackToRoutine[m_pathBackToRoutine.Count - 1].m_worldPosition, percentage);

                        if (percentage >= 1)
                        {
                            m_pathBackToRoutine.Remove(m_pathBackToRoutine[m_pathBackToRoutine.Count - 1]);
                            m_startPosGL = transform.position;
                            m_currentLerpTime = 0f;
                        }
                    }

                    else
                    {
                        // Reset
                        if (m_eGuardType == EGuardType.GT_Static)
                        {
                            m_eGuardState = EGuardState.GS_IsRotatingToStartPos;
                        }

                        else
                        {
                            // reset values
                            m_currentLerpTime = 0f;
                            m_lastPosition = new Vector3(0f, 0f, -1f);
                            m_startRotGL = transform.localRotation;
                            m_startPosGL = transform.position;
                            m_dirToNextCP = (m_guard.m_checkPointList[m_guard.m_currentCheckPointNumber].transform.localPosition - transform.localPosition).normalized;
                            m_eGuardState = EGuardState.GS_IsTurning;
                        }
                    }
                    break;

                case EGuardState.GS_IsRotatingToStartPos:
                    //animator.SetBool("IsWalking", false);
                    animator.StopPlayback();
                    PlayerRot = transform.localRotation;
                    PlayerTrans = Matrix4x4.Rotate(Quaternion.Lerp(PlayerRot, m_guard.m_startRot, Time.smoothDeltaTime * m_rotationSpeed).normalized);
                    transform.localRotation = PlayerTrans.rotation;

                    if (PlayerRot.eulerAngles.z <= m_guard.m_startRot.eulerAngles.z + 0.1f && PlayerRot.eulerAngles.z >= m_guard.m_startRot.eulerAngles.z - 0.1f)
                    {
                        m_currentLerpTime = 0f;
                        m_lastPosition = new Vector3(0f, 0f, -1f);
                        m_startRotGL = transform.localRotation;
                        m_startPosGL = transform.position;
                        m_eGuardState = EGuardState.GS_IsStanding;
                    }
                    break;

                case EGuardState.GS_IsDying:
                    //animator.SetBool("IsWalking", false);
                    animator.StopPlayback();
                    //Debug.Log("About to die...");

                    //at the end of animation
                    if (m_ps.isStopped)
                        gameObject.SetActive(false);

                    break;
            }

            //Debug.Log("State: " + (int)m_eGuardState);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            //if player is shadow stepping
            //die
            if (collision.transform.GetComponent<PlayerController>().m_isShadowStepping && !GameManager.Instance.IsInitiatingDeathSequence())
            {
                m_ps.Play();
                GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1]++;
               
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                m_meshRenderer.enabled = false;
                m_eGuardState = EGuardState.GS_IsDying;
                GameManager.Instance.m_playerIsBeingFollowed = false;
                if (m_isShooter)
                    Destroy(transform.GetChild(2).gameObject);
                //Debug.Log("Guard is dying...");
            }
            if(collision.CompareTag("Player") && !collision.transform.GetComponent<PlayerController>().m_isShadowStepping)
            {
                GameManager.Instance.InititateDeathSequence();
            }
        }
    }

    public EGuardState GetGuardState()
    {
        return m_eGuardState;
    }

    public void ResetValues()
    {
        m_currentLerpTime = 0f;
        m_startPosGL = m_guard.m_checkPointList[0].transform.position;

        if (m_eGuardType != EGuardType.GT_Static)
        {
            m_eGuardState = EGuardState.GS_IsMoving;
        }
        else
            m_eGuardState = EGuardState.GS_IsStanding;

        m_lastPosition = new Vector3(0, 0, -1);
        m_dir = 1;
        m_guard.m_currentCheckPointNumber = 1;
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;
        m_meshRenderer.enabled = true;
        animator.SetBool("IsShooting",false);
    }

#if UNITY_EDITOR
    public void CreateCheckPoint()
    {
        GameObject cp = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Enemy Prefabs/CheckPointGuard.prefab", typeof(GameObject));
        GameObject go = PrefabUtility.InstantiatePrefab(cp, transform.parent.transform.parent.GetChild(1)) as GameObject;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localPosition = new Vector3(5, 5, 0);
        m_guard.m_checkPointList.Add(go);

        go.transform.parent = this.transform.parent.transform.parent.GetChild(1);
    }

    public void RemoveCheckPoint()
    {
        if (m_guard.m_checkPointList.Count > 0)
        {
            DestroyImmediate(m_guard.m_checkPointList[m_guard.m_checkPointList.Count - 1]);
            m_guard.m_checkPointList.Remove(m_guard.m_checkPointList[m_guard.m_checkPointList.Count - 1]);
        }
    }
#endif
}
