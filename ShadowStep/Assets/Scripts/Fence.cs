﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fence : MonoBehaviour
{
    List<Collider2D> m_circleColliderList;
    [SerializeField]
    internal float m_radius = 5f;

    private void Start()
    {
        m_circleColliderList = new List<Collider2D>();

        CircleCollider2D[] circleCollider2Ds = GetComponentsInChildren<CircleCollider2D>();
        foreach (CircleCollider2D i in circleCollider2Ds)
        {
            m_circleColliderList.Add(i);
        }
    }


    // Update is called once per frame
    void Update()
    {
        Collider2D col = Physics2D.OverlapCircle(transform.position, m_radius);

        if(col)
        {
            PlayerController player = col.GetComponent<PlayerController>();
            if (player)
            {
                if (player.m_isShadowStepping)
                {
                    foreach (CircleCollider2D i in m_circleColliderList)
                    {
                        i.enabled = false;
                    }
                }
                else
                {
                    foreach (CircleCollider2D i in m_circleColliderList)
                    {
                        i.enabled = true;
                    }
                }
            }

        }
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, m_radius);
    }
}
