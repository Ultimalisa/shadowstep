﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public int m_gridX;
    public int m_gridY;

    public bool m_isWall;
    public Vector3 m_worldPosition;

    public Node m_parent;

    public int m_gCost; // distance between current node and start node
    public int m_hCost; // distance to goal from current node

    public int m_fCost { get { return m_gCost + m_hCost; } }

    public Node(bool isWall, Vector3 pos, int gridX, int gridY)
    {
        m_isWall = isWall;
        m_worldPosition = pos;
        m_gridX = gridX;
        m_gridY = gridY;
    }
}
