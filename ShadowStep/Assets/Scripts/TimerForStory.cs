﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TimerForStory : MonoBehaviour
{
    [HideInInspector]
    public static int m_timerForStory;
    public static bool m_sequenceIsFinished = false;

  
    // Update is called once per frame
    void Update()
    {
        m_timerForStory++;
        if(PlayerController.m_interacted==true || m_timerForStory>1570)
        {
            gameObject.SetActive(false);
            m_sequenceIsFinished = true;
        }
    }
}
