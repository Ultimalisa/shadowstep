﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoalBehavior : MonoBehaviour
{
   
   
    [HideInInspector]
    public static bool m_levelend = false;
    private void Start()
    {
        if (m_levelend == true)
        {
            m_levelend = false;
        }
    }

    private void Update()
    {
        if (m_levelend == false && TimerForStory.m_sequenceIsFinished)
        {
            GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1] += Time.deltaTime;

        }
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D collider)
    {
       // PlayerPrefs.SetFloat("playerTime" + LevelManager.Instance.GetCurrentLevelNumber(), GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]);
       // PlayerPrefs.SetFloat("playerTimeMin" + LevelManager.Instance.GetCurrentLevelNumber(), GameManager.Instance.m_progress.TimeInLevelMin[LevelManager.Instance.GetCurrentLevelNumber() - 1]);
        //  GameManager.Instance.m_isGameRunning = !GameManager.Instance.m_isGameRunning;
        if (collider.gameObject.CompareTag("Player"))
        {
            
            GameObject clearScore = GameManager.Instance.m_clearScore;
            clearScore.SetActive(true);
            m_levelend = true;
            clearScore.transform.GetChild(5).gameObject.GetComponent<Text>().text = Mathf.Floor(GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber()-1]/60f).ToString("0")+":"+ (GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]%60).ToString("00");
            clearScore.transform.GetChild(3).gameObject.GetComponent<Text>().text = GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber()-1].ToString();
            clearScore.transform.GetChild(4).gameObject.GetComponent<Text>().text = GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber()-1].ToString();
           
            GameManager.Instance.m_progress.SetValues();
            clearScore.GetComponent<ClearLogic>().GiveMedals();
        }
        
    }
}
