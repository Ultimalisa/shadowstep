﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchützeLogic : MonoBehaviour
{
    public GameObject m_bullet;
    private FieldOfView m_fieldOfView;
    public Rigidbody2D m_rb;
    private float m_bulletSpeed = 10f;
    public float m_cooldown = 1f;

    private void Start()
    {
        m_fieldOfView = GetComponent<FieldOfView>();
    }
    // Update is called once per frame
   
    void FixedUpdate()
    {


        if (m_cooldown < -1f)
        {
            m_cooldown = 1f;
        }

        if (m_cooldown == 1f)
        {
            m_bullet.transform.position = transform.position;
        }
        if (m_cooldown <= 0f)
        {
              m_bullet.SetActive(true);
              m_rb.velocity = m_bullet.transform.up * m_bulletSpeed;
        }
          else m_bullet.SetActive(false);


        

        if (m_fieldOfView.m_visibleTarget == null && m_cooldown>0f)
        {
            m_cooldown = 1f;
        }
    }
    void LateUpdate()
    {
        if (m_fieldOfView.m_visibleTarget != null || m_cooldown<=0f)
        {
            m_cooldown -= Time.deltaTime;
        }
    }
}
