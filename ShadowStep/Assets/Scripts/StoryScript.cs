﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StoryScript : MonoBehaviour
{


    public float delay = 0.05f;
    private string fullText;
    private string currentText = "";

    private void Start()
    {
        fullText = "Danger lies within the shadows of this world."+"\n"+"\n"+"Dark creatures slip into the realm of the living from beyond the veil."+"\n"+ "Driven by instinct they lurk and drag their prey to the lands of the dead.";
    }
    // Update is called once per frame
    void Update()
    {
        if (TimerForStory.m_timerForStory > 600)
        {
            if(currentText=="")
            StartCoroutine(ShowText2());
        }
        if (TimerForStory.m_timerForStory > 1050)
        {
           
            gameObject.SetActive(false);
        }
           
    }
    IEnumerator ShowText2()
    {
        for (int i = 0; i <= fullText.Length; i++)
        {
            currentText = fullText.Substring(0, i);
            this.transform.GetChild(0).GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
    }
}
