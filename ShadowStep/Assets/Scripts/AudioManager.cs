﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    internal List<AudioSource> m_musicList;
    [SerializeField]
    internal List<AudioSource> m_specialEffectsList;
    [HideInInspector]
    public float m_musicVolume;
    [HideInInspector]
    public float m_sfxVolume;

    public AudioSource m_musicSourceMain;
   


    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.m_audioManager = this;
        m_specialEffectsList = new List<AudioSource>();
        m_specialEffectsList.Clear();

        m_musicSourceMain.Play();
       
       
       
    }

    public void SetVolumeEffects(float volume)
    {
        for(int i=0;i<m_specialEffectsList.Count;i++)
            m_specialEffectsList[i].volume = volume;
    }

    public void SetVolumeMusic(float volume)
    {
        
        m_musicList[0].volume = volume;
        m_musicList[1].volume = volume;
    }
}
