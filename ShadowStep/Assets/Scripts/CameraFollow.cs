﻿
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothspeed;
    public Vector3 offset;
   
    private bool Switched = false;

    private void Start()
    {
        AudioSource[] audioS = GetComponentsInChildren<AudioSource>();
        if(audioS.Length > 0)
        {
            foreach (AudioSource i in audioS)
            {
                GameManager.Instance.m_audioManager.m_specialEffectsList.Add(i);
            }
        }
        
    }
    void FixedUpdate()
    {
        transform.position = target.position + offset;
       
    }
    private void Update()
    {
        if (GameManager.Instance.m_player.m_allowShadowStepping == true)
        {
            if (!Switched)
            {
                Switched = true;
                transform.GetChild(2).gameObject.SetActive(true);
                transform.GetChild(0).GetComponent<AudioSource>().Play();
            }

        }
        else
        {
            transform.GetChild(2).gameObject.SetActive(false);
            Switched = false;
        }
    }
}
