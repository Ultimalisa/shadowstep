﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearLogic : MonoBehaviour
{
    public Button m_exitButton;
    public Button m_nextButton;

    private void Awake()
    {
        m_nextButton.onClick.AddListener(LoadNextLevel);
        m_exitButton.onClick.AddListener(ExitToMain);
    }

    private void Update()
    {
        if (LevelManager.Instance.m_isShowingMouse)
        {
            LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
        }
        else
        {
            if (LevelManager.Instance.m_eventSystem.currentSelectedGameObject == null)
            {
                if(m_nextButton.enabled)
                    LevelManager.Instance.m_eventSystem.SetSelectedGameObject(m_nextButton.gameObject);
            }
        }
    }

    public void GiveMedals()
    {
       
            if (GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1] >= 1 && GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1] < 2)
            {
                transform.GetChild(6).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[1];
                PlayerPrefs.SetInt("KillWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 1);
            }
            if (GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1] >= 2 && GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1] < 3)
            {
                transform.GetChild(6).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[2];
                PlayerPrefs.SetInt("KillWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 2);
            }
            if (GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber() - 1] >= 3)
            {
                transform.GetChild(6).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[3];
                PlayerPrefs.SetInt("KillWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 3);
            }


            if (GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1] >= 2 && GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1] < 4)
            {
                transform.GetChild(7).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[1];
                PlayerPrefs.SetInt("DeathWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 1);

            }
            if (GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1] >= 1 && GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1] < 2)
            {
                transform.GetChild(7).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[2];
                PlayerPrefs.SetInt("DeathWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 2);

            }
            if (GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber() - 1] < 1)
            {
                transform.GetChild(7).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[3];
                PlayerPrefs.SetInt("DeathWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 3);

            }

            if ((GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]) > 110f)
            {
                transform.GetChild(8).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[1];
                PlayerPrefs.SetInt("TimeWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 1);

            }
            if ((GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]) >= 70f && (GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]) <= 110f)
            {
                transform.GetChild(8).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[2];
                PlayerPrefs.SetInt("TimeWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 2);

            }
            if ((GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber() - 1]) < 70f)
            {
                transform.GetChild(8).gameObject.GetComponent<Image>().sprite = GameManager.Instance.m_gearSpriteList[3];
                PlayerPrefs.SetInt("TimeWheel" + LevelManager.Instance.GetCurrentLevelNumber(), 3);

            }
        
    }

    void LoadNextLevel()
    {
        GoalBehavior.m_levelend = false;
        if(LevelManager.Instance.GetCurrentLevelNumber()!=GameManager.Instance.GetNumberOfLastLevel())
        {
            if (GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber()] != 0f ||
                GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber()] != 0 ||
                GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber()] != 0)
            {

                GameManager.Instance.m_progress.TimeInLevel[LevelManager.Instance.GetCurrentLevelNumber()] = 0f;
                GameManager.Instance.m_progress.playerDeaths[LevelManager.Instance.GetCurrentLevelNumber()] = 0;
                GameManager.Instance.m_progress.enemiesKilled[LevelManager.Instance.GetCurrentLevelNumber()] = 0;
            }

            LevelManager.Instance.LoadSceneWithIndex(LevelManager.Instance.GetCurrentLevelNumber() + 1);
        }
        else LevelManager.Instance.LoadScene("LevelSelect");

        gameObject.SetActive(false);
    }

    void ExitToMain()
    {
        LevelManager.Instance.LoadScene("MainMenu");
        LevelManager.Instance.m_eventSystem.SetSelectedGameObject(null);
        gameObject.SetActive(false);
    }
}
