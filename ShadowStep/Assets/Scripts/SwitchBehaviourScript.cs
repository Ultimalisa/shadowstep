﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class SwitchBehaviourScript : MonoBehaviour
{
   
   
    private bool m_switched = false;
    public Transform m_cp;
    internal AudioSource m_switchSound;
    public AudioSource m_engineSound;
    SpriteRenderer m_spriteRenderer;
    public Animator m_animator;


    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_switchSound = GetComponent<AudioSource>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(m_switchSound);
        GameManager.Instance.m_audioManager.m_specialEffectsList.Add(m_engineSound);


    }
    private void Update()
    {
        GameObject Lightfield = transform.GetChild(0).gameObject;
        if (PlayerController.m_butPressed == true)
        {
            if (!m_switched && GameManager.Instance.m_player.m_generatorName == gameObject.name)
            {
                m_engineSound.Stop();
                for(int i=0;i<transform.GetChild(1).childCount;i++)
                {
                    transform.GetChild(1).GetChild(i).GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.Instance.m_crystalOffSprite;
                }
                
                transform.GetChild(2).gameObject.SetActive(true);
                m_animator.SetBool("IsDead", true);
                m_switchSound.Play();
                Lightfield.SetActive(false);
               
                m_switched = true;
                GameManager.Instance.m_player.m_playerStartPos = new Vector2(m_cp.localPosition.x,m_cp.localPosition.y);
            }
           
        }
    }

   
       
            
        


       
    
}
