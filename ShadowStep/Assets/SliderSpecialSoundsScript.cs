﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderSpecialSoundsScript : MonoBehaviour
{
    Slider m_slider;
    // Start is called before the first frame update
    void Start()
    {
        m_slider = GetComponent<Slider>();
        m_slider.onValueChanged.AddListener(delegate { SetVolume(m_slider.value); }) ;
        //m_slider.value = GameManager.Instance.m_audioManager.m_sfxVolume;

        if (PlayerPrefs.GetFloat("SFXVolume") > 0f)
        {
            m_slider.value = PlayerPrefs.GetFloat("SFXVolume");
            GameManager.Instance.m_audioManager.SetVolumeEffects(m_slider.value);
        }
    }



    void SetVolume(float vol)
    {
        //GameManager.Instance.m_audioManager.m_sfxVolume = vol;
        //Debug.Log("vol: " + GameManager.Instance.m_audioManager.m_sfxVolume);
        GameManager.Instance.m_audioManager.SetVolumeEffects(vol);

        PlayerPrefs.SetFloat("SFXVolume", vol);
    }

}
