# Shadowstep

Projektphase I/IV

this is a top-down single player stealth game, where the player 
has to dodge lasers and take care of guards, that follow or shoot at you
once you've been seen, in order to reach the end of the level.

**mechanics:**

* press hold the dodge button and use the left analog stick (or the wasd keys) to control the direction
and release the button to execute the dash (Shadow Step)

* you can only execute the dash when you are standing in a shadow field (hence Shadow Step)

* don't go inside light fields, the light will hurt your character

* turn off the lights with their associated light generator (marked by the color of their crystal stones)

* control the volume of the background music and sound effects in the pause menu

**interesting to look at:**

the AI state machine of the guards can be viewed in /Assets/Scripts/Guards/GuardLogic.cs

you can take a closer look at the player controls in /Assets/Scripts/PlayerController.cs

happy gaming :)